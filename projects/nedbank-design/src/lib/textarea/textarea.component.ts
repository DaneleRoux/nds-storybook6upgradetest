import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ui-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss']
})
export class TextareaComponent implements OnInit {
  
  textarea = '';

  /**
  * The textarea's name
  */
  @Input() name: string;

  /**
  * The input aria name.
  */
 @Input() arialabel: string;

  /**
  * The textarea's rows
  */
  @Input() rows: number;

  /**
  * Does the input have a label?
  */
  @Input() hasLabel: boolean = true;

  /**
  * The textarea's maximum length.
  */
 @Input() maxLength: number = 50;

  /**
  * The textarea's limit reached text.
  */

  @Input() limitReachedText: string = "Charater limit reached";
  
  /**
  * The label's text.
  */
  @Input() labelText: string = 'Default label';

  /**
   * Does the label have a tooltip?
   */
  @Input() hideTooltip: boolean = true;

  /**
   * Does the tooltip have a heading?
   */
  @Input() hasHeading: boolean;

  /**
   * The tooltip's heading.
   */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltip's text.
  */
  @Input() tooltipText: string;

  /**
   * The disabled state the input.
   */
  @Input() disabled: boolean;

  /**
  * Valid input.
  */
  @Input() isValid: boolean;

  /**
  * Invalid input.
  */
  @Input() notValid: boolean;

  /**
   * Does input has Helper text.
   */
  @Input() hashelperText: boolean = true;

  /**
  * Helper text 
  */
  @Input() helperText: string = 'Default helper text';

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;
  

  inputChanged(event) {
    
    const element = event.target;
    const inputValue = event.target.value;
    const inputValueLenght = event.target.value.length;

    if (inputValue) {
      element.classList.add('valid');
    }
    else {
      element.classList.remove('valid');
    }
    
  }

  constructor() { }

  ngOnInit() {}

}
