import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ui-icon',
  templateUrl: 'icons.component.html' ,
  styleUrls: ['./icons.component.scss']
})
export class IconsComponent implements OnInit {

  /**
  * The name of the icon.
  */
  @Input() iconName: string = '';

  /**
  * The size of the icon - 16px, 24px or 32px.
  */
  @Input() iconSize: string = '';

  /**
  * The icon's color.
  */
  @Input() iconColor: string = '';

  constructor() { }

  ngOnInit() {
  }

}
