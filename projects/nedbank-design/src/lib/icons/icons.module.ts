import { NgModule } from '@angular/core';
import { IconsComponent } from './icons.component';
import { SharedModule } from './../shared.module';


@NgModule({
  declarations: [IconsComponent],
  imports: [SharedModule
  ],
  exports: [IconsComponent]
})
export class IconsModule { }
