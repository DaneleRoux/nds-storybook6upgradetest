import { Component, OnInit, Input, ViewEncapsulation, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ui-checkbox',
  templateUrl: 'checkbox.component.html' ,
  styleUrls: ['./checkbox.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CheckboxComponent implements OnInit {

  /**
  * The checkbox's 'id' attribute.
  */
  @Input() checkboxId: string;

  /**
  * The checkbox 'for' attribute.
  */
  @Input() checkboxFor: string;

  /**
  * The checkbox checked value.
  */
  @Input() hasChecked: boolean;

  /**
  * Applies a 15px margin between checkboxes in a group.
  */
  @Input() groupItemMargin: boolean;

  /**
  * Does the input have a label?
  */
  @Input() hasLabel: boolean = false;

  /**
   * The label's text.
   */
  @Input() labelText: string = 'Default label';

    /**
   * The label's text.
   */
   @Input() checkboxText: string = 'Option 1';

  /**
  * Does the label have a tooltip?
  */
  @Input() hideTooltip: boolean = true;

  /**
   * Does the tooltip have a heading?
   */
  @Input() hasHeading: boolean;

  /**
   * The tooltip's heading.
   */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltip's text.
  */
  @Input() tooltipText: string;

  /**
  * Does input have Helper text.
  */
  @Input() hashelperText: boolean = false;

  /**
   * Helper text.
   */
  @Input() helperText: string = 'Default helper text';

  /**
  * Disabled state.
  */  
  @Input() disabled: boolean;

  /**
  * Valid input.
  */
  @Input() isValid: boolean;

  /**
  * Invalid input.
  */
  @Input() notValid: boolean;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  /**
  * The checkbox checked emitted
  */
  @Output() checkboxCheckedUpdate = new EventEmitter();

  constructor() { }

  ngOnInit() {
    
  }

  checkboxChecked(event) {

    this.hasChecked = !this.hasChecked;

    //Emit event and toggle checked
    this.checkboxCheckedUpdate.emit({event: event, checked: this.hasChecked});
  }

}
