import { NgModule } from '@angular/core';
import { CheckboxComponent } from './checkbox.component';
import { SharedModule } from './../shared.module';
import { FormLabelModule } from '../form-label/form-label.module';
import { FormHelperTextModule } from '../form-helper-text/form-helper-text.module';
import { TooltipModule } from '../tooltip/tooltip.module';

@NgModule({
  declarations: [CheckboxComponent],
  imports: [ SharedModule, FormLabelModule, FormHelperTextModule, TooltipModule
  ],
  exports: [CheckboxComponent]
})
export class CheckboxModule { }
