import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NedbankDesignComponent } from './nedbank-design.component';

describe('NedbankDesignComponent', () => {
  let component: NedbankDesignComponent;
  let fixture: ComponentFixture<NedbankDesignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NedbankDesignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NedbankDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
