import { Component, Input, OnInit, AfterViewInit, ViewChild, ComponentFactoryResolver, Output, EventEmitter, HostListener, ElementRef} from '@angular/core';
import { StepDirective } from './stepper.directive';
import { StepComponent } from './step.component';
import { StepItem } from './step.items';


@Component({
	selector: 'ui-stepper',
	templateUrl: './stepper.component.html',
	styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {

	@Input() steps: StepItem[];
	@Input() stickyWithAlert: boolean;
	@Input() mainStepperButton = true;
	@Input() SaveModalLink = true;
	@Input() SideStepper = true;
	@Input() Counter = true;

	stepButton = true;
	stepButtonText: any;
	currentStepIndex = -1;
	allSteps: any = [];
	mainSteps: any = [];
	mainStepNames: any = [];
	disabledMainStep: any;
	disabled = false;
	substepMainDisabled: any;
	totalMainSteps: number;
	stepId = 1;
	stepUid;
	currentMainStep = 0;
	active: boolean;
	mobileNavOpen = false;

	// Save and Cancel modal values
	stepperSave: boolean = false;
	stepperCancel: boolean = false;

	sticky: boolean = false;
	content: boolean = false;
	wrapperOverflow: boolean = false;
	elementPosition: any;

	@ViewChild('stickyMenu', {static: false}) menuElement: ElementRef;


	// Save and Cancel event emitters (From within modal component)
	@Output() modalCheck = new EventEmitter();


	@ViewChild(StepDirective, {static: true}) stepHost: StepDirective;


	constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

	//////////////////////
	// Click/ Init functions
	/////////////////////

	ngOnInit() {

		// Create allSteps array
		let i;
		for (i = 0; i < this.steps.length; i++) {
			this.allSteps.push(this.steps[i].data);

			// Create mainSteps array from allSteps
			if (this.allSteps[i].mainStep){
				this.mainSteps.push(this.allSteps[i].mainStep);
				this.totalMainSteps = this.mainSteps.length;
			}
		}

		// Run load component on button click
		this.loadComponent();
	
	}

	@HostListener('window:resize', ['$event'])
		onResize(event) {
			this.checkWindow();
		}


	checkWindow(){
		var mq = window.matchMedia( "(max-width: 768px)" );
		if (mq.matches) {
			setTimeout(() => {
				// Get width and height of the window excluding scrollbars
				const windowWidth = document.documentElement.clientWidth;
				const arrows = document.querySelectorAll(".dropdown-arrow");
				[].forEach.call(arrows, (el) => {
					el.style.left = windowWidth - 129 + 'px';
				});	
			}, 100);
		}
		else {
			setTimeout(() => {
				const arrows = document.querySelectorAll(".dropdown-arrow");
				[].forEach.call(arrows, (el) => {
					el.style.left = 123 +  'px';
				});	
			}, 100);
		}
	}


	// Get progess bar height
	getProgress(){
		setTimeout(() => {
			const progress = document.querySelector('.progress-bar-side .progress') as HTMLElement;
			const sidebarElement = document.querySelector('.sidebar') as HTMLElement;
			const ActiveClass = document.querySelector('.activeClass') as HTMLElement;
			const sidebartop = sidebarElement.getBoundingClientRect().top;
			const currentLinktop = ActiveClass.getBoundingClientRect().top;
			const currentLinkHeight = ActiveClass.getBoundingClientRect().height;
			const difference = currentLinktop - sidebartop;

			//Apply height to side progress bar
			progress.style.height = difference + (currentLinkHeight / 2)  +  'px';



			//Mobile///
			//////////
			const progressMobile = document.querySelector('.progress-bar-top .progress') as HTMLElement;
			const mainStepsCount = document.querySelectorAll('.main-level').length;

			var mq = window.matchMedia( "(max-width: 768px)" );
			if (mq.matches) {
				progress.style.height = difference + (currentLinkHeight / 2)  - 20 +   'px';
			}

			const stepLink = document.querySelectorAll('.sidebar-link');

			[].forEach.call(stepLink, (el) => {
				if(el.querySelector("span").classList.contains("activeClass") && el.querySelector("span").classList.contains("last-item")){
					//Apply height to side progress bar
					progress.style.height = 100 +  '%';
				}	
			});

			[].forEach.call(stepLink, (el) => {

				if(el.classList.contains("main-level")){
					if(el.querySelector("span").classList.contains("activeClass")){
						const currentStep = el.getAttribute("value");
						progressMobile.style.width =  currentStep / mainStepsCount  * 100 + '%';
					}	
				}	
			});

		}, 100);
	}

	// On button click to run loadComponent and adjust arrow dropdowns
	getSteps() {

		//Reset dropdown arrows
		const reset =  document.querySelectorAll('.dropdown-arrow');
		[].forEach.call(reset, (node, i) => {
			// node.classList.add('nlsg-icon-arrow-down');
			node.classList.remove('activeClassArrow');
		});

		setTimeout(() => {
			const currentStep = this.steps[this.currentStepIndex];
			if (currentStep.data.mainStep == true) {

				//Reset dropdown arrows
				const reset =  document.querySelectorAll('.nlsg-icon');
				[].forEach.call(reset, (node, i) => {
					node.classList.add('nlsg-icon-arrow-down');
					node.classList.remove('nlsg-icon-arrow-up');
				});

				this.stepId = currentStep.data.order;
				this.stepUid = currentStep.data.uniqueId;

				const currentArrow =  document.querySelector('#'+this.stepUid);
				currentArrow.querySelector('.nlsg-icon').classList.remove('nlsg-icon-arrow-down');
				currentArrow.querySelector('.nlsg-icon').classList.add('nlsg-icon-arrow-up');
			}
			else {
				const mainStepStayActive =  document.querySelector('#'+currentStep.data.mainStepUniqueId);
				mainStepStayActive.querySelector('.dropdown-arrow').classList.add('activeClassArrow');
			}

		}, 5);

		this.loadComponent();
		
	}

	// On link click and load to get current index and update counter
	getStepsSideNav(index, event) {

		//Reset dropdown arrows
		const reset =  document.querySelectorAll('.dropdown-arrow');
		[].forEach.call(reset, (node, i) => {
			// node.classList.add('nlsg-icon-arrow-down');
			node.classList.remove('activeClassArrow');
		});

		const stepItem = this.steps[index];
		this.stepButtonText = stepItem.data.buttonText;

		this.getProgress();

		if (stepItem.data.mainStep) {

			//Reset dropdown arrows
			const reset =  document.querySelectorAll('.nlsg-icon');
			[].forEach.call(reset, (node, i) => {
				node.classList.add('nlsg-icon-arrow-down');
				node.classList.remove('nlsg-icon-arrow-up');
			});

			this.stepId = stepItem.data.order;
			this.stepUid = stepItem.data.uniqueId;

			const currentArrow =  document.querySelector('#'+this.stepUid);
			currentArrow.querySelector('.nlsg-icon').classList.remove('nlsg-icon-arrow-down');
			currentArrow.querySelector('.nlsg-icon').classList.add('nlsg-icon-arrow-up');

			//Reset dropdown arrows end
			this.dropdownCheck(index);

			if (this.stepId <= this.totalMainSteps ) {
				this.currentMainStep = this.stepId;
				this.currentStepIndex = index;
			}
		}
		else {

			if (this.stepId <= this.totalMainSteps ) {
				this.currentMainStep = this.stepId;
				this.currentStepIndex = index;
			}

			this.currentStepIndex = index;

			setTimeout(() => {
				const mainStepStayActive =  document.querySelector('#'+stepItem.data.mainStepUniqueId);
				mainStepStayActive.querySelector('.dropdown-arrow').classList.add('activeClassArrow');

			}, 5);
		}
		this.componentFactory(stepItem);
	}


	// Launch modals
	launchModal(event) {
		
		// Save or Cancel link pressed?
		const currentModal = event.target.parentElement.parentElement.getAttribute("id");

		// Save link pressed
		if(currentModal === 'save'){
			// Emit true to modalCheck emitter
			this.modalCheck.emit(true);
		}

		else {
			// Emit true to modalCheck emitter
			this.modalCheck.emit(false);
		}
	}

	// Open/close mobile menu
	mobileNav(){

		this.checkWindow();

		if(this.mobileNavOpen == false){
			this.mobileNavOpen = true;
		}
		else {
			this.mobileNavOpen = false;
		}
	}

	//////////////////////
	// Internal functions
	/////////////////////

	dropdownCheck(index){ 
		if (this.allSteps[index]){
			let i;
			for (i = 0; i < this.steps.length; i++) {

				if (this.allSteps[index].stepName === this.allSteps[i].MainstepName){
					this.allSteps[i].dropdown = true;
					this.allSteps[index].mainDropdown = true;
				}
				else {
					this.allSteps[i].dropdown = false;
				}
			}
		}
	}

	// Set active on current item and run component factory to create instances
	componentFactory(stepItem) {
		this.resetActive();

		if (stepItem){
			stepItem.data.active = true;
		}

		const componentFactory = this.componentFactoryResolver.resolveComponentFactory(stepItem.component);
		const viewContainerRef = this.stepHost.viewContainerRef;
		viewContainerRef.clear();
		const componentRef = viewContainerRef.createComponent(componentFactory);

		(<StepComponent>componentRef.instance).data = stepItem.data;
	}

	// Get current index and update counter
	loadComponent() {
		this.currentStepIndex = (this.currentStepIndex + 1) % this.steps.length;
		const stepItem = this.steps[this.currentStepIndex];

		this.stepButtonText = stepItem.data.buttonText;

		if (stepItem.data.mainStep) {
			this.stepId = stepItem.data.order;

			this.dropdownCheck(this.currentStepIndex );

			if (this.stepId <= this.totalMainSteps ) {
				this.currentMainStep = this.stepId;

			}
			else {
					this.currentMainStep = 1;
			}
		}

		this.getProgress();
		this.componentFactory(stepItem);
	}

	// Reset active data to false
	resetActive(){
		let i;
		for (i = 0; i < this.steps.length; i++) {
			this.steps[i].data.active = false;
		}
	}  
}
