import { NgModule } from '@angular/core';
import { StepperComponent } from './stepper.component';
import { CommonModule } from '@angular/common';
import { StepDirective } from './stepper.directive';
import { ButtonModule } from '../button/button.module';
import { ModalModule } from '../modal/modal.module';
import { HyperlinkModule } from '../hyperlink/hyperlink.module';


@NgModule({
  declarations: [StepperComponent, StepDirective],
  imports: [
    CommonModule, ButtonModule, ModalModule, HyperlinkModule
  ],
  exports: [StepperComponent]
})
export class StepperModule { }
