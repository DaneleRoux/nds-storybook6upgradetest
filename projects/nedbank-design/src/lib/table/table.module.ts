import { NgModule } from '@angular/core';
import { TableComponent } from './table.component';
import { CheckboxModule } from '../checkbox/checkbox.module'
import { SharedModule } from './../shared.module';


@NgModule({
  declarations: [TableComponent],
  imports: [SharedModule, CheckboxModule],
  exports: [TableComponent],
})
export class TableModule { }
