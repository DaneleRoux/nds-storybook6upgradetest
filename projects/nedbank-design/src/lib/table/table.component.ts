import { Component, OnInit, Input,Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { KeyValue } from '@angular/common';

@Component({
  selector: 'ui-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class TableComponent implements OnInit {

  /**
  * The label's text.
  */
  @Input() checkboxText: string = '';

  /**
  * The label's text.
  */
  @Input() currentCheck: boolean = false;

  /**
  * The label's text.
  */
  @Input() mainCheckoxChecked: boolean = false;


  /**
  * The table header's data.
  */
  @Input() tableId: string;


  /**
  * The table header's data.
  */
  @Input() tableHeaders: any;

  /**
  * The table accesibility caption.
  */
  @Input() tableCaption: string;

  /**
  * The table row's data.
  */
  @Input() rowDatas: any;

  /**
  * If sorting table.
  */
  @Input() tableSorter: boolean = true;

  /**
  * If nested table.
  */
  @Input() nestedTables: boolean = false;

  /**
  * If small table.
  */
  @Input() smallTables: boolean = false;

  /**
  * If editable table.
  */
  @Input() editableTables: boolean = false;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
   * Margin bottom 30px.
   */
  @Input() marginBottomMd: boolean;

  /**
   * Margin bottom 60px.
   */
  @Input() marginBottomLg: boolean;

  /**
   * Margin bottom 0px.
   */
  @Input() marginBottomNone: boolean;


  @Output() rowDataEmitted = new EventEmitter();



    ngOnInit() {

      setTimeout(() => {
      
        const tdsValue = document.querySelectorAll('.tdvalues .value');

        [].forEach.call(tdsValue, (el, index) => {
          const yourString = el.innerHTML;
          const tdValueWidth = el.getBoundingClientRect().width;
          const tooltip =  el.parentElement.querySelector('.table-tooltip');
          const prevEl =  el.previousElementSibling;

          if (tooltip != null) {
            const tooltipWidth = tooltip.offsetWidth;
            const halfTooltip = tooltipWidth / 2 ;

            if (index % 2 === 0) {
              tooltip.style.top =  0 + 'px';
              tooltip.style.left =  10 + (tdValueWidth / 2) - halfTooltip + 'px';

            } else {
              tooltip.style.top =  0 + 'px';
              tooltip.style.right = 13 + (tdValueWidth / 2)  - halfTooltip + 'px';
            }
          }

          if (prevEl != null) {
            if (prevEl.classList.contains('table-input')) {
              prevEl.nextElementSibling.style.display = 'none';
            }
            if (prevEl.classList.contains('stacked-title') && prevEl.innerHTML === '') {
                prevEl.style.display = 'none';
            }
          }

          // Check if its a number
          if (!isNaN(parseFloat(yourString)) && isFinite(yourString)) {
            // Check if its a negative number
            if (yourString < 0) {
              el.classList.add('negative-table-number');
            }
            // Check if its a positive number
            else {
              el.classList.add('positive-table-number');
            }
          }
        });
      }, 10);
    }

    toggleDropdown(event) {

      const element = event.target;

      if (element.nextElementSibling.classList.contains('show-dropdown')) {
        element.nextElementSibling.classList.remove('show-dropdown');
      }
      else {

        const removeActive = document.querySelectorAll('.more-actions-container');
        [].forEach.call(removeActive, (el) => {
          el.classList.remove('show-dropdown');
        });
        element.nextElementSibling.classList.add('show-dropdown');
        element.classList.toggle('openDropdown');
      }
    }

    toggleTable(event) {

      const element = event.target;
      if (element.parentElement.parentElement.nextElementSibling.classList.contains('hiddenTable')) {

        const removeActive = document.querySelectorAll('.expand-table-container ');
        const removeActiveArrows = document.querySelectorAll('.expand-table-row');

        [].forEach.call(removeActive, (el) => {
          el.classList.add('hiddenTable');
        });

        [].forEach.call(removeActiveArrows, (el) => {
          el.classList.remove('openDropdown');
        });

        element.parentElement.parentElement.nextElementSibling.classList.remove('hiddenTable');
        element.classList.add('openDropdown');
      }
      else {

        element.parentElement.parentElement.nextElementSibling.classList.add('hiddenTable');
        element.classList.remove('openDropdown');
      }
    }

    isCurrentChecked(rowIndex, valuesEmitted){

        if(valuesEmitted.checked === true){
           this.getRowData(rowIndex);
        }
    } 

    getRowData(rowIndex){
      this.rowDataEmitted.emit(this.rowDatas[rowIndex]);
  }

    isMainChecked(valuesEmitted){

      if(valuesEmitted.checked === false){
        this.mainCheckoxChecked = false;
        this.toggleAllCheckboxesSortableTable(valuesEmitted);
      }
      else {
        this.mainCheckoxChecked = true;
        this.toggleAllCheckboxesSortableTable(valuesEmitted);

      }
    } 

    toggleAllCheckboxesSortableTable(valuesEmitted) {
      
        var i, x, currentId = this.tableId;
        x = document.querySelectorAll('#' + currentId + ' .nlsg-c-checkbox input');


        if(valuesEmitted.checked === false){
          this.currentCheck = false;
          for (i = 1; i < x.length; i++) {          
            x[i].checked = false;
            x[i].removeAttribute("checked", "");
          }
        }
        else {
          this.rowDataEmitted.emit(this.rowDatas);
          this.currentCheck = true;
          for (i = 1; i < x.length; i++) {
            x[i].checked = true;
            x[i].setAttribute("checked", "checked");
          }
        }
    }

    originalOrder = (a: KeyValue <number, string>, b: KeyValue <number, string>): number => {
      return 0;
    }

    headerClicked(ev, clickedItem) {
      const hasClicked = ev.target;
      const rowsArray = this.rowDatas;

      // Checks objects and nested objects
      const iterate = (obj) => {
        Object.keys(obj).forEach(key => {

          // If it has nested objects interate through them
          if (typeof obj[key] === 'object') {
            iterate(obj[key]);
          }

          function mycomparator(a, b) {

            if (typeof a[clickedItem][0] === 'number') {
              return a[clickedItem][0] - b[clickedItem][0];
            }

            if (typeof a[clickedItem][0] !== 'number') {
              if (a[clickedItem][2] !== undefined ) {
                  return (a[clickedItem][2].stacked[0].toString().toLowerCase() > b[clickedItem][2].stacked[0].toString().toLowerCase())
                  ? 1 : ((b[clickedItem][2].stacked[0].toString().toLowerCase() > a[clickedItem][2].stacked[0].toString().toLowerCase())
                  ? -1 : 0);
              }
              else {
                return (a[clickedItem].toString().toLowerCase() > b[clickedItem].toString().toLowerCase())
                ? 1 : ((b[clickedItem].toString().toLowerCase() > a[clickedItem].toString().toLowerCase())
                ? -1 : 0);
              }
            }
          }
          // If the item clicked and the datas key match
          if (key === clickedItem) {
            // If the item clicked is clicked for the first or second time
            if (hasClicked.classList.contains('clicked')) {
              hasClicked.classList.remove('clicked');
              rowsArray.reverse(mycomparator);
            } else {
              // sort of current selection
              hasClicked.classList.add('clicked');
              rowsArray.sort(mycomparator);
            }
          }
        });
      };
      iterate(rowsArray);
    }
}

