import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ui-range-slider',
  templateUrl: './range-slider.component.html',
  styleUrls: ['./range-slider.component.scss']
})
export class RangeSliderComponent implements OnInit {

  /**
  * Does the input have a label?
  */
  @Input() hasLabel: boolean = true;

  /**
  * The label's text.
  */
  @Input() labelText: string;

  /**
  * The input aria name.
  */
  @Input() arialabel: string;

  /**
  * Does the label have a tooltip?
  */
  @Input() hideTooltip: boolean = true;

  /**
   * Does the tooltip have a heading?
   */
  @Input() hasHeading: boolean;

  /**
   * The tooltip's heading.
   */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltip's text.
  */
  @Input() tooltipText: string;

  /**
   * The Inputs default value
   */
  @Input() defaultValue: number;

  /**
  * The Inputs minimum value
  */
  @Input() min: number;

  /**
  * The Inputs maximum value
  */
  @Input() max: number;

  /**
  * The disabled state the input.
  */
  @Input() disabled: boolean = false;

  /**
  * Invalid input.
  */
  @Input() notValid: boolean;

  /**
  * Current input value.
  */
  @Input() currentValue: number  = 0;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  /**
  * The slider's value emitted
  */
  @Output() valueUpdated = new EventEmitter();


  constructor() {}

  ngOnInit() {
    

    // Wrapper, Input and bullet elements
    const rangeSliderWrapper: any = document.getElementById('wrapper');
    const rangeSlider: any = document.getElementById('rs-range-line');
    const rangeBullet: any = document.getElementById('rs-bullet');

    // Event listener on input and run showSliderValue function
    rangeSlider.addEventListener('input', showSliderValue, false);

    // Get value and move bullet
    function showSliderValue() {
      // Add value to range bullet
      rangeBullet.innerHTML = 'R' + rangeSlider.value;

      // Get bullet position
      const bulletPosition = (rangeSlider.value * 100 / rangeSlider.max);

      if (bulletPosition === 0 ) {
        rangeBullet.style.left = bulletPosition - 1.4 + '%';
      }
      // else if (bulletPosition > 0 && bulletPosition < 15 ) {
      //   rangeBullet.style.left = bulletPosition - 2.4 + '%';
      // }
      else if (bulletPosition > 0 && bulletPosition < 25 ) {
        rangeBullet.style.left = bulletPosition - 3.4 + '%';
      }
      else if (bulletPosition > 25 && bulletPosition < 50 ) {
        rangeBullet.style.left = bulletPosition - 5.4 + '%';
      }
      else if (bulletPosition === 50 ) {
        rangeBullet.style.left = bulletPosition - 6.4 + '%';
      }
      else if (bulletPosition > 50 && bulletPosition < 75 ) {
        rangeBullet.style.left = bulletPosition - 7.4 + '%';
      }
      else if (bulletPosition > 75 && bulletPosition < 100 ) {
        rangeBullet.style.left = bulletPosition - 8.4 + '%';
      }
      else if (bulletPosition === 100 ) {
        rangeBullet.style.left = bulletPosition - 9.4 + '%';
      }
      
      // Add gradient during event listener
      applyFill(rangeSliderWrapper.querySelector('input'));

    }

    // // Add gradient onInit
    setTimeout(function(){
        showSliderValue();
        applyFill(rangeSliderWrapper.querySelector('input'));
     }, 30);

    function applyFill(SliderRangeInput) {

      // Nedbank colour settings for range slider
      const settings = {
        fill: '#78be20',
        background: '#bbb'
      };

      // Get percentage moved and apply gradient green
      const percentage = 100 * (SliderRangeInput.value) / (SliderRangeInput.max);
      const bg = `linear-gradient(90deg, ${settings.fill} ${percentage}%, ${settings.background} ${percentage + 0.1}%)`;

      // Apply to range input
      if (/MSIE 11/i.test(navigator.userAgent)) {
        SliderRangeInput.style.background = 'transparent';
      }
      if (/Edge/.test(navigator.userAgent)) {
        SliderRangeInput.style.background = 'transparent';
      }
      else {
        SliderRangeInput.style.background = bg;
      }
    }
  }

  getValue(){
    const rangeBullet: any = document.getElementById('rs-bullet');
    const getHtmlValue = rangeBullet.innerHTML;
    this.valueUpdated.emit(getHtmlValue);
  }


}
