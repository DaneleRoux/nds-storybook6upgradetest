import { NgModule } from '@angular/core';
import { ListComponent } from './list.component';
import { SharedModule } from './../shared.module';


@NgModule({
  declarations: [ListComponent],
  imports: [SharedModule
  ],
  exports: [ListComponent]
})
export class ListModule { }
