import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'ui-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  /**
  * Set data variable to create lists
  */
  @Input() listItems: any;

  /**
  * To create a unordered list
  */
  @Input() unorderedList: boolean = true;

  /**
  * To create a ordered list
  */
  @Input() orderedList: boolean = false;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  constructor() { }

  ngOnInit() {
  }

}
