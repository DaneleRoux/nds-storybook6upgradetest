import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockAlertComponent } from './block-alert.component';

describe('BlockAlertComponent', () => {
  let component: BlockAlertComponent;
  let fixture: ComponentFixture<BlockAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
