import { TestBed } from '@angular/core/testing';

import { BlockAlertService } from './block-alert.service';

describe('BlockAlertService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BlockAlertService = TestBed.get(BlockAlertService);
    expect(service).toBeTruthy();
  });
});
