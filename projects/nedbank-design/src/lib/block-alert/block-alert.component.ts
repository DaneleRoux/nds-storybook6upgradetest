import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ui-block-alert',
  templateUrl: 'block-alert.component.html',
  styleUrls: ['./block-alert.component.scss']

})

export class BlockAlertComponent implements OnInit {

  /**
  * Is this an 'Informative' alert?
  */
  @Input() infoBlockAlert: boolean = false;

  /**
   * Is this a 'Warning' alert?
   */
  @Input() warningBlockAlert: boolean = false;

  /**
   * Is this a 'Success' alert?
   */
  @Input() successBlockAlert: boolean = false;

  /**
   * Is this an 'Error' alert?
   */
  @Input() errorBlockAlert: boolean = false;

  /**
   * Is this a 'fixed' alert?
   */
  @Input() fixedAlert: boolean = false;

  /**
  * Is the alert hidden?
  */
  @Input() alertVisible: boolean = true;

  /**
  * Does the alert only have 1 line of text (ie. no body text)?
  */
  @Input() singleLineBlockAlert: boolean = false;

  /**
  * Alert heading text.
  */
  @Input() alertHeading: string = '';

  /**
  * Alert body text.
  */
  @Input() alertBody: string = '';

  /**
  * Does the alert have links?
  */
  @Input() hasLinks: boolean = false;

  /**
  * 1st alert link text. 
  */
  @Input() link1Text: string;

  /**
  * 1st alert link URL. 
  */
  @Input() link1URL: string = '#';

  /**
  * 2nd alert link text.
  */
  @Input() link2Text: string;

  /**
  * 2nd alert link URL. 
  */
  @Input() link2URL: string = '#';

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;
  

  /**
   * The eventEmitters to update hide and show values + Which BTN was pressed
   */
  @Output() hideShowBlockAlertUpdate = new EventEmitter();
  @Output() linkClickedUpdate = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }


  closeAlert() {
    //Set the global block Alert value to false / closed
    this.hideShowBlockAlertUpdate.emit(false);
  }

  checkLink(linkValue) {
    //Emit link string for check
    this.linkClickedUpdate.emit(linkValue);
  }
}
