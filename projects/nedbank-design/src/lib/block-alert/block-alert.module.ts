import { NgModule } from '@angular/core';
import { SharedModule } from './../shared.module';
import { BlockAlertComponent } from './block-alert.component';

@NgModule({
  declarations: [BlockAlertComponent],
  imports: [
    SharedModule
  ],
  exports: [BlockAlertComponent]
})
export class BlockAlertModule { }
