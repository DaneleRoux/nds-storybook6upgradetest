import { NgModule } from '@angular/core';
import { PaginationComponent } from './pagination.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from './../shared.module';

@NgModule({
  declarations: [PaginationComponent],
  imports: [
    NgxPaginationModule, SharedModule
  ],
  exports: [PaginationComponent]
})
export class PaginationModule { }
