import { Component, Input } from '@angular/core';

@Component({
  selector: 'ui-pagination',
  templateUrl: 'pagination.component.html' ,
  styleUrls: ['./pagination.component.scss']
})

export class PaginationComponent {
  /**
  * The configuration shared by the pagination component and the paged content.
  */
  @Input() paginationSettings: any;

  /**
  * The number of page links to show.
  */
  @Input() public maxSize: number = 7;

  constructor() {
  } // constructor ends

  onPageChange(event){
    this.paginationSettings.currentPage = event;
  }
}
