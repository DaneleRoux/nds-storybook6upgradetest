import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';  
import { FileUploadService } from './file-upload.service';

@Component({
  selector: 'ui-file-upload',
  templateUrl: 'file-upload.component.html' ,
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {

  /**
  * Does the input have a label?
  */
  @Input() hasLabel: boolean = true;

  /**
  * The label's text.
  */
  @Input() labelText: string = 'Default label';

  /**
   * Does the label have a tooltip?
   */
  @Input() hideTooltip: boolean = true;

  /**
   * Does the tooltip have a heading?
   */
  @Input() hasHeading: boolean;

  /**
   * The tooltip's heading.
   */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltip's text.
  */
  @Input() tooltipText: string;

  /**
  * Is this a multiple file upload?
  */
  @Input() multiUpload: boolean = true;

  /**
  * What is the file size limit, in MB?
  */
  @Input() fileSizeLimit: number;

  /**
  * What files are accepted? (e.g: .gif, .jpg, .png, .doc)
  */
  @Input() fileFormats: string = '';

  /**
  * The URL of the upload directory.
  */
  @Input() uploadUrl: string = '';

  /**
  * Does input have Helper text?
  */
  @Input() hashelperText: boolean = true;

  /**
   * Helper text .
   */
  @Input() helperText: string = 'Default helper text';

  /**
  * Helper text error message.
  */
  @Input() helperTextErrorMessage: string = 'Default helper text';

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  /**
  * Current file uploaded and its data.
  */
  @Output() fileUpdated = new EventEmitter();

  hasError: boolean = false;

  @ViewChild("fileUpload", {static: false}) fileUpload: ElementRef; files  = [];


  constructor(private uploadService: FileUploadService) { }
  
  ngOnInit() {
    this.uploadService.SERVER_URL = this.uploadUrl;
  }

  uploadFile(file) {
    const formData = new FormData();
    formData.append('file', file.data);
    file.inProgress = true;
    this.uploadService.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.data.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
          // console.log(event.body);
        }
      });
  }

  private uploadFiles() {
    this.fileUpload.nativeElement.value = '';
    this.files.forEach(file => {
      var currentFileSize = file.data.size;
      var currentFileSizeToMB = ((currentFileSize / 1024) / 1024);
      if(currentFileSizeToMB > this.fileSizeLimit) {
        this.hasError = true;
        // Don't upload the file
      }
      else {
        this.uploadFile(file);
      }
    });
    this.fileUpdated.emit(this.files);
  }

  onClick() {
    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.onchange = () => {
    for (let index = 0; index < fileUpload.files.length; index++)
    {
      const file = fileUpload.files[index];
      this.files.push({ data: file, name, inProgress: false, progress: 0});
    }
      this.uploadFiles();
      // console.log(this.files);
    };
    fileUpload.click();
  }

  // Delete files
  removeSelectedFile(index){
    // console.log(index);
    this.files.splice(index, 1);
    this.hasError = false;
  } 
}
