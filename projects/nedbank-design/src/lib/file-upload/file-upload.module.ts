import { NgModule } from '@angular/core';
import { FileUploadComponent } from './file-upload.component';
import { TruncateName } from './truncate.pipe';
import { SharedModule } from './../shared.module';
import { FormLabelModule } from '../form-label/form-label.module';
import { FormHelperTextModule } from '../form-helper-text/form-helper-text.module';
import { TooltipModule } from '../tooltip/tooltip.module';

@NgModule({
  declarations: [FileUploadComponent, TruncateName],
  imports: [SharedModule, FormLabelModule, FormHelperTextModule, TooltipModule],
  exports: [FileUploadComponent, TruncateName]
})
export class FileUploadModule { }
