import { TestBed } from '@angular/core/testing';

import { InlineAlertService } from './inline-alert.service';

describe('InlineAlertService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InlineAlertService = TestBed.get(InlineAlertService);
    expect(service).toBeTruthy();
  });
});
