import { NgModule } from '@angular/core';
import { InlineAlertComponent } from './inline-alert.component';
import { SharedModule } from './../shared.module';


@NgModule({
  declarations: [InlineAlertComponent],
  imports: [ SharedModule
  ],
  exports: [InlineAlertComponent]
})
export class InlineAlertModule { }
