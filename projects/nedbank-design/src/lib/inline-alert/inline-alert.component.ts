import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ui-inline-alert',
  templateUrl: 'inline-alert.component.html',
  styleUrls: ['./inline-alert.component.scss']
})
export class InlineAlertComponent implements OnInit {

  /**
  * Is this an 'Informative' alert?
  */
  @Input() infoInlineAlert: boolean = false;

  /**
   * Is this a 'Warning' alert?
   */
  @Input() warningInlineAlert: boolean = false;

  /**
   * Is this a 'Success' alert?
   */
  @Input() successInlineAlert: boolean = false;
  
  /**
   * Is this an 'Error' alert?
   */
  @Input() errorInlineAlert: boolean = false;

  /**
  * Is this alert visible?
  */
  @Input() showAlert: boolean;

  /**
  * Alerts body text
  */
  @Input() alertBody: string = '';

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  constructor() { }

  ngOnInit() { }



}
