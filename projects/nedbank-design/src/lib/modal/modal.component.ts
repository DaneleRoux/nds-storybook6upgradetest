import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';

@Component({
  selector: 'ui-modal',
  templateUrl: 'modal.component.html' ,
  styleUrls: ['./modal.component.scss']
  
})
export class ModalComponent implements OnInit {

  /**
  * The tooltips shown.
  */
  @Input() hideTooltip: boolean = true;

  /**
  * The tooltips direction.
  */
  @Input() hasHeading: boolean = false;

  /**
  * The tooltips heading.
  */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltips text.
  */
  @Input() tooltipText: string;

  /**
  * Is the modal visible?
  */
  @Input() isShowModal: boolean = false;

  /**
  * Modal id
  */
  @Input() modalId: string;

  /**
  * Does the modal have an illustration?
  */
  @Input() hasIllustration: boolean = true;

  /**
  * The modal's illustration URL.
  */
  @Input() modalIllustrationUrl: string = '';

  /**
  * The modal's heading.
  */
  @Input() modalHeading: string = '';

  /**
  * The modal's content.
  */
  @Input() modalText: string = '';

  /**
  * The content of the button.
  */
  @Input() buttonText: string = '';

  /**
  * Is it a secondary button?
  */
  @Input() secondary: boolean = false;

  /**
  * The content of the button.
  */
  @Input() secondaryButtonText: string = '';

  /**
  * Does the button have an icon?
  */
  @Input() secondaryIconButton: boolean = false;

  /**
  * Is the button disabled?
  */
  @Input() disabled: boolean = false;

  /**
  * The eventEmitters to update hide and show values + Which BTN was pressed
  */
  @Output() hideShowModalUpdate = new EventEmitter();
  @Output() btnClickedUpdate = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  closeModal() {
    //Set the global modal value to false / closed
    this.hideShowModalUpdate.emit(false);
  }

  primaryBtn() {
    //Emit primary string for check
    this.btnClickedUpdate.emit('Primary');
  }

  secondaryBtn() {
    //Emit secondary string for check
    this.btnClickedUpdate.emit('Secondary');
  }
  
}
