import { NgModule } from '@angular/core';
import { ModalComponent } from './modal.component';
import { ButtonModule } from '../button/button.module';
import { SharedModule } from './../shared.module';

@NgModule({
  declarations: [ModalComponent],
  imports: [ButtonModule, SharedModule],
  exports: [ModalComponent],
})
export class ModalModule { }
