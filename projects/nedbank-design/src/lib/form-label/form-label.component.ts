import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ui-form-label',
  templateUrl: 'form-label.component.html' ,
  styleUrls: ['./form-label.component.scss']
})
export class FormLabelComponent implements OnInit {
  
  
  /**
  * The label's text.
  */
  @Input() labelText: string = 'Default label';

  /**
  *  Margin Top 15px. 
  */
  @Input() marginTopSm: boolean;

  /**
  * Margin Top 30px.
  */
  @Input() marginTopMd: boolean;

  /**
  * Margin Top 60px.
  */
  @Input() marginTopLg: boolean;

  /**
  * Margin Top 0px.
  */
  @Input() marginTopNone: boolean;

  /**
  * Does the label have a tooltip?
  */
  @Input() hideTooltip: boolean = true;

  /**
  * Does the tooltip have a heading?
  */
  @Input() hasHeading: boolean;

  /**
  * The tooltip's heading.
  */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltip's text.
  */
  @Input() tooltipText: string;

  /**
   * The disabled state the label.
   */
  @Input() disabled: boolean;

  /**
  * Invalid label.
  */
  @Input() notValid: boolean;

  

  constructor() { }

  ngOnInit() {
  }

}
