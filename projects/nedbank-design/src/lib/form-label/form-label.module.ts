import { NgModule } from '@angular/core';
import { FormLabelComponent } from './form-label.component';
import { SharedModule } from './../shared.module';
import { TooltipModule } from '../tooltip/tooltip.module';

@NgModule({
  declarations: [FormLabelComponent],
  imports: [SharedModule, TooltipModule],
  exports: [FormLabelComponent]
})
export class FormLabelModule { }
