import { TestBed } from '@angular/core/testing';

import { FormLabelService } from './form-label.service';

describe('FormLabelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FormLabelService = TestBed.get(FormLabelService);
    expect(service).toBeTruthy();
  });
});
