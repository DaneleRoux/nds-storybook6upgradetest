import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ui-tag',
  templateUrl: 'tag.component.html' ,
  styleUrls: ['./tag.component.scss']
})
export class TagComponent implements OnInit {
  
  remove_tag: boolean = false;
  
  /**
  * The tag's body text.
  */
  @Input() tagText: string = "";

  /**
  * Does the tag have a close icon?
  */
  @Input() hasCloseIcon: boolean = true;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  constructor() { }

  ngOnInit() {
  }

  removeTag() {
    this.remove_tag = true;
  }

}
