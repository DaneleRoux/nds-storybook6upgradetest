import { NgModule } from '@angular/core';
import { TagComponent } from './tag.component';
import { SharedModule } from './../shared.module';


@NgModule({
  declarations: [TagComponent],
  imports: [SharedModule
  ],
  exports: [TagComponent]
})
export class TagModule { }
