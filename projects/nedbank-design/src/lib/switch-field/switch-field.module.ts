import { NgModule } from '@angular/core';
import { SwitchFieldComponent } from './switch-field.component';
import { SharedModule } from './../shared.module';
import { FormLabelModule } from '../form-label/form-label.module';
import { FormHelperTextModule } from '../form-helper-text/form-helper-text.module';
import { TooltipModule } from '../tooltip/tooltip.module';

@NgModule({
  declarations: [SwitchFieldComponent],
  imports: [SharedModule, FormLabelModule, FormHelperTextModule, TooltipModule],
  exports: [SwitchFieldComponent]
})
export class SwitchFieldModule { }
