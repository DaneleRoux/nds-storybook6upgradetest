import { TestBed } from '@angular/core/testing';

import { SwitchFieldService } from './switch-field.service';

describe('SwitchFieldService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SwitchFieldService = TestBed.get(SwitchFieldService);
    expect(service).toBeTruthy();
  });
});
