import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ui-switch-field',
  templateUrl: './switch-field.component.html',
  styleUrls: ['./switch-field.component.scss']
})
export class SwitchFieldComponent implements OnInit {


  /**
  * Does the input have a label?
  */
  @Input() hasLabel: boolean = true;

  /**
  * The label's text.
  */
  @Input() labelText: string;

  /**
  * Does the label have a tooltip?
  */
  @Input() hideTooltip: boolean = true;

  /**
  * Does the tooltip have a heading?
  */
  @Input() hasHeading: boolean = false;

  /**
  * The tooltip's heading.
  */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltip's text.
  */
  @Input() tooltipText: string;

  /**
  * Left switch checked
  */
  @Input() leftChecked: boolean = true;

  /**
  * Left switch text
  */
  @Input() leftText: string;

  /**
  * Middle switch shown
  */
  @Input() middleSwitch: boolean = false;

  /**
  * Middle switch checked
  */
  @Input() middleChecked: boolean = false;

  /**
  * Middle switch text
  */
  @Input() middleText: string;

  /**
  * Right switch checked
  */
  @Input() rightChecked: boolean = false;

  /**
  * Right switch text
  */
  @Input() rightText: string;

  /**
  * Does input has Helper text.
  */
  @Input() hashelperText: boolean = true;

 /**
  * Helper text 
  */
  @Input() helperText: string = 'Default helper text';

  /**
  * The disabled state the input.
  */
  @Input() disabled: boolean = false;

  /**
  * Valid input.
  */
  @Input() isValid: boolean = false;

  /**
  * Invalid input.
  */
  @Input() notValid: boolean = false;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  /**
  * switch vales emitted
  */
  @Output() switchValueUpdate = new EventEmitter();


  constructor() { }

  ngOnInit() {
  }

  switchValueCheck(event) {

    const currentSwitch = event.target.getAttribute("value");

    // console.log(currentSwitch);

    if(currentSwitch === 'Left'){
      this.switchValueUpdate.emit('left');
    }
    if(currentSwitch === 'Middle') {
      this.switchValueUpdate.emit('middle');
    }
    if(currentSwitch === 'Right') {
      this.switchValueUpdate.emit('right');
    }  
  }

}
