import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ui-card',
  templateUrl: 'card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  /**
  * The cards' data set.
  */
  @Input() cards: any;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;
  
  /**
  * The cards' link value emitted
  */
  @Output() linkClickedUpdate = new EventEmitter();

  constructor() { }

  ngOnInit() { }

  checkLink(linkValue) {
    //Emit link string for check
    this.linkClickedUpdate.emit(linkValue);
  }

}
