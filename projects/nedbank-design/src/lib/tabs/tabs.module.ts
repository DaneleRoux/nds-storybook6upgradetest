import { NgModule } from '@angular/core';
import { TabsComponent } from './tabs.component';
import { RouterModule} from '@angular/router';
import { SharedModule } from './../shared.module';


@NgModule({
  declarations: [TabsComponent],
  imports: [SharedModule, RouterModule
  ],
  exports: [TabsComponent]
})
export class TabsModule { }
