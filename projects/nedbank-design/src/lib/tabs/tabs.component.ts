  import { Component, OnInit, Input } from '@angular/core';

  @Component({
  selector: 'ui-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
  })
  export class TabsComponent implements OnInit {

  /**
  *  Tabs data.
  */  
  @Input() tabDatas: any;

  defaultActive = true;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  constructor() {}

  ngOnInit() {}

  toggleActive(event) {

    const removeActive = document.querySelectorAll('li');

    [].forEach.call(removeActive, (el) => {
    // el.classList.remove('active');
    el.classList.remove('focus');
    });

    const element = event.target;
    // element.parentElement.classList.toggle('active');
    element.parentElement.scrollIntoView({behavior: 'smooth', block: 'center', inline: 'center'});
  }
  onTab(ev) {
    const removeActive = document.querySelectorAll('li');
    [].forEach.call(removeActive, (el) => {
      // el.classList.remove('active');
      el.classList.remove('focus');
    });

    const element = event.target  as HTMLElement;
    if (element.parentElement.nextElementSibling) {
      // element.parentElement.nextElementSibling.classList.toggle('active');
      element.parentElement.nextElementSibling.classList.toggle('focus');
      element.parentElement.nextElementSibling.scrollIntoView({behavior: 'smooth', block: 'center', inline: 'center'});
    }
    else {
      element.parentElement.classList.add('focus');
      // element.parentElement.classList.add('active');
    }
  }
}
