import { NgModule } from '@angular/core';
import { SearchSelectComponent } from './search-select.component';
import { SearchPipe } from './search-select-filter-pipe';
import { HighlightPipe } from './search-select-hightlight-pipe';
import { SharedModule } from './../shared.module';
import { FormLabelModule } from '../form-label/form-label.module';
import { FormHelperTextModule } from '../form-helper-text/form-helper-text.module';
import { TooltipModule } from '../tooltip/tooltip.module';

@NgModule({
  declarations: [
    SearchSelectComponent,
    SearchPipe,
    HighlightPipe
  ],
  imports: [
    SharedModule, FormLabelModule, FormHelperTextModule, TooltipModule
  ],
  exports: [SearchSelectComponent, SearchPipe, HighlightPipe]
})
export class SearchSelectModule { }
