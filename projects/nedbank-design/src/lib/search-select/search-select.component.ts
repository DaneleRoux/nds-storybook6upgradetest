import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, HostListener  } from '@angular/core';

@Component({
  selector: 'ui-search-select',
  templateUrl: './search-select.component.html',
  styleUrls: ['./search-select.component.scss']
})
export class SearchSelectComponent implements OnInit {

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (this.eRef.nativeElement.contains(event.target)) {
      if (this.closed === false){
        this.closed = false;
      }
    } else {
      this.closed = true;
    }
  }

  constructor(private eRef: ElementRef) {
  }

/**
* Does the input have a label?
*/
@Input() hasLabel: boolean = true;

/**
* The label's text.
*/
@Input() labelText: string = 'Default label';

/**
 * Does the label have a tooltip?
 */
@Input() hideTooltip: boolean = true;

/**
 * Does the tooltip have a heading?
 */
@Input() hasHeading: boolean;

/**
 * The tooltip's heading.
 */
@Input() tooltipHeadingText: string;

/**
* The tooltip's text.
*/
@Input() tooltipText: string;

/**
 * The select's value text.
 */
@Input() value: string = '';

/**
 * The select's placeholder text.
 */
@Input() selectPlaceholder: string = '';

/**
 * The data for the options
 */
@Input() options: any;

selectOtion: string = '';

/**
* Does input has Helper text.
*/
@Input() hashelperText: boolean = true;

/**
 * Helper text 
 */
@Input() helperText: string = 'Default helper text';

/**
 * The disabled state of select.
 */
@Input() disabled: boolean;

/**
 * Valid select.
 */
@Input() isValid: boolean;

/**
* Not a valid select.
*/
@Input() notValid: boolean;


@Input() selected: any;

/**
*  Margin bottom 15px. 
*/
@Input() marginBottomSm: boolean;

/**
* Margin bottom 30px.
*/
@Input() marginBottomMd: boolean;

/**
* Margin bottom 60px.
*/
@Input() marginBottomLg: boolean;

/**
* Margin bottom 0px.
*/
@Input() marginBottomNone: boolean;


@Output() selectedChange: EventEmitter<any> = new EventEmitter();

searchSelectTerm: any;

closed: boolean = true;

  ngOnInit() {
    this.selected = '';
  }

  select(option: any): void {
    this.selected = option;
    this.selectedChange.emit(option);
    if(this.closed == false) {
      this.closed = true;
    }
    this.isValid = true;
  }

  blurFunction(){
    if(this.closed === false) {
      this.closed = true;
    }
  }

  @ViewChild('searchSelectField', {static: false}) searchSelectField;

  clearicon(){
    this.searchSelectTerm = '';
    this.searchSelectField.nativeElement.focus();
  }

  openClose(): void {
    if(this.closed === true) {
      this.closed = false
    }
  }

  arrowkeyLocation = 0;

  @ViewChild('optionsContainer', {static: false}) optionsContainer;

  keyDown(event: KeyboardEvent) {
    const selectOptions = document.querySelectorAll('.option');
    const hoveredOption = document.querySelector('.hovered');
    const searchSelectField = document.querySelectorAll('.searchSelectField');

    switch (event.keyCode) {
      case 38: // Up arrow
        if(this.arrowkeyLocation == 0) {
          // If it is at the first list item, do nothing.
        } else {
          this.arrowkeyLocation--;
          if(hoveredOption.getBoundingClientRect().height >= 45){
            this.optionsContainer.nativeElement.scrollTop = this.arrowkeyLocation * 44;
          } else {
            this.optionsContainer.nativeElement.scrollTop = this.arrowkeyLocation * 44;
          }
        }
      break;
      
      case 40: // Down arrow
        if(this.arrowkeyLocation == this.options.length - 1) {
          // If it is at the last list item, do nothing.
        } else {
          this.arrowkeyLocation++;
          if(hoveredOption.getBoundingClientRect().height >= 45){
            this.optionsContainer.nativeElement.scrollTop = this.arrowkeyLocation * 44;
          } else {
            this.optionsContainer.nativeElement.scrollTop = this.arrowkeyLocation * 44;
          }
        }
      break;

      case 13: // Enter
        
        [].forEach.call(selectOptions, (el) => {
          if(el.parentElement.classList.contains('open') && el.classList.contains('hovered')) {
            el.click();
            [].forEach.call(searchSelectField, (inputel) => {
              inputel.blur();
              this.arrowkeyLocation = 0;
            });
          }
        });
      break;

      case 32: // Space
        [].forEach.call(selectOptions, (el) => {
          if(el.parentElement.classList.contains('open') && el.classList.contains('hovered')) {
            el.click();
            [].forEach.call(searchSelectField, (inputel) => {
              inputel.blur();
              this.arrowkeyLocation = 0;
            });
          }
        });
      break;

    }//switch
   
 }//keyDown

  keyUpTab(event) {
    const selectElement = event.target;
    event = event || window.event;
    var keycode = event.charCode || event.keyCode;
    if(selectElement.parentElement.classList.contains('disabled')) {
    //Do nothing
    }
    else {
      if(keycode === 9){
        this.openClose();
      }
    }

  }//keyUpTab

  keyDownTab(event) {
    const selectElement = event.target;
    event = event || window.event;
    var keycode = event.charCode || event.keyCode;
    // Tab
    if(keycode === 9){
      if(selectElement.parentElement.classList.contains('disabled')) {
        // Do nothing
      }
      else {
        this.openClose();
      }
    }
 }//keyDownTab

}
