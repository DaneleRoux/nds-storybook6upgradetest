import { TestBed } from '@angular/core/testing';

import { SearchSelectService } from './search-select.service';

describe('SearchSelectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchSelectService = TestBed.get(SearchSelectService);
    expect(service).toBeTruthy();
  });
});
