import { TestBed } from '@angular/core/testing';

import { NedbankDesignService } from './nedbank-design.service';

describe('NedbankDesignService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NedbankDesignService = TestBed.get(NedbankDesignService);
    expect(service).toBeTruthy();
  });
});
