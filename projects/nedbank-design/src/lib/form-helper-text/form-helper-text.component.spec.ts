import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormHelperTextComponent } from './form-helper-text.component';

describe('FormHelperTextComponent', () => {
  let component: FormHelperTextComponent;
  let fixture: ComponentFixture<FormHelperTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormHelperTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormHelperTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
