import { TestBed } from '@angular/core/testing';

import { FormHelperTextService } from './form-helper-text.service';

describe('FormHelperTextService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FormHelperTextService = TestBed.get(FormHelperTextService);
    expect(service).toBeTruthy();
  });
});
