import { NgModule } from '@angular/core';
import { FormHelperTextComponent } from './form-helper-text.component';
import { SharedModule } from './../shared.module';


@NgModule({
  declarations: [FormHelperTextComponent],
  imports: [ SharedModule
  ],
  exports: [FormHelperTextComponent]
})
export class FormHelperTextModule { }
