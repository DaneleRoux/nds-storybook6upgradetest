import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ui-form-helper-text',
  templateUrl: 'form-helper-text.component.html' ,
  styleUrls: ['./form-helper-text.component.scss']
})
export class FormHelperTextComponent implements OnInit {

  /**
  * Does input has Helper text.
  */
  @Input() hashelperText: boolean = true;

  /**
   * Helper text 
   */
  @Input() helperText: string = 'Default helper text';

  /**
   * Is the helper text disabled?
   */
  @Input() disabled: boolean = false;

  /**
   * Is the helper text invalid?
   */
  @Input() notValid: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
