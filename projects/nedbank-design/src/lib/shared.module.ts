import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [CommonModule, FormsModule, BrowserModule, ReactiveFormsModule, HttpClientModule, BrowserAnimationsModule],
  declarations: [],
  exports: [
    CommonModule, FormsModule, BrowserModule, ReactiveFormsModule,
    HttpClientModule, BrowserAnimationsModule
  ]
})
export class SharedModule {}
