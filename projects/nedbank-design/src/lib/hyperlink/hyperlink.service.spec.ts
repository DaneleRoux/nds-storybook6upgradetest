import { TestBed } from '@angular/core/testing';

import { HyperlinkService } from './hyperlink.service';

describe('HyperlinkService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HyperlinkService = TestBed.get(HyperlinkService);
    expect(service).toBeTruthy();
  });
});
