import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ui-hyperlink',
  templateUrl: 'hyperlink.component.html' ,
  styleUrls: ['./hyperlink.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HyperlinkComponent implements OnInit {

  /**
  * The link's text.
  */
  @Input() linkText: string;

  /**
  * The link's URL.
  */
  @Input() linkUrl: string;

  /**
  * The name of link's icon.
  */
  @Input() iconName: string;

  /**
   * Does the label have a tooltip?
   */
  @Input() hideTooltip: boolean = true;

  /**
   * Does the tooltip have a heading?
   */
  @Input() hasHeading: boolean;

  /**
   * The tooltip's heading.
   */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltip's text.
  */
  @Input() tooltipText: string;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  constructor() { }

  ngOnInit() {
  }

}
