import { NgModule } from '@angular/core';
import { HyperlinkComponent } from './hyperlink.component';
import { SharedModule } from './../shared.module';
import { TooltipModule } from '../tooltip/tooltip.module';

@NgModule({
  declarations: [HyperlinkComponent],
  imports: [SharedModule, TooltipModule],
  exports: [HyperlinkComponent]
})
export class HyperlinkModule { }
