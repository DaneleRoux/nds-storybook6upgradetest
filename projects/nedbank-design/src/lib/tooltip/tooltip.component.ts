import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ui-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TooltipComponent implements OnInit {

  /**
  * The tooltips text.
  */
 @Input() tooltipText: string;

  /**
  * The tooltips heading.
  */
 @Input() tooltipHeadingText: string;

  /**
  * Tooltip bottom center position.
  */
 @Input() bottomCenter: boolean;
 
  /**
  * Tooltip top center position.
  */
  @Input() topCenter: boolean;

   /**
  * The tooltips direction.
  */
  @Input() hasHeading: boolean = false;

   /**
  * The tooltips shown.
  */
  @Input() hideTooltip: boolean = true;
 
  constructor() { }
  ngOnInit() {}
}
