import { NgModule } from '@angular/core';
import { TooltipComponent } from './tooltip.component';
import { SharedModule } from './../shared.module';


@NgModule({
  declarations: [TooltipComponent],
  imports: [SharedModule
  ],
  exports: [TooltipComponent]
})
export class TooltipModule { }
