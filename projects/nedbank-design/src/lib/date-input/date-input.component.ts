import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ui-date-input',
  templateUrl: 'date-input.component.html' ,
  styleUrls: ['./date-input.component.scss'], 
  encapsulation: ViewEncapsulation.None
})
export class DateInputComponent implements OnInit {

  /**
  * Does the input have a label?
  */
  @Input() hasLabel: boolean = true;
  
  /**
  * The label's text.
  */
  @Input() labelText: string;

  /**
  * The input aria name.
  */
 @Input() arialabel: string;

  /**
  * Does the label have a tooltip?
  */
  @Input() hideTooltip: boolean = true;

  /**
  * Does the tooltip have a heading?
  */
  @Input() hasHeading: boolean;

  /**
  * The tooltip's heading.
  */
  @Input() tooltipHeadingText: string;

  /**
   * The tooltip's text.
   */
  @Input() tooltipText: string;

  /**
  * Does input have Helper text.
  */
  @Input() hashelperText: boolean = true;

  /**
   * Helper text.
   */
  @Input() helperText: string;

  /**
  * The disabled state the input.
  */
  @Input() disabled: boolean;

  /**
   * Valid input.
   */
  @Input() isValid: boolean;

  /**
   * Invalid input.
   */
  @Input() notValid: boolean;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  constructor() {  }

  ngOnInit() {}

  numberCheckMax2(event) {
    const element = event.target;
    const inputValue = element.value;

    if (inputValue.length === 2) {
      element.parentElement.nextElementSibling.querySelector('input').focus();
    }
  }
}
