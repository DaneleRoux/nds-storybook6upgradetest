import { TestBed } from '@angular/core/testing';

import { DateInputService } from './date-input.service';

describe('DateInputService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DateInputService = TestBed.get(DateInputService);
    expect(service).toBeTruthy();
  });
});
