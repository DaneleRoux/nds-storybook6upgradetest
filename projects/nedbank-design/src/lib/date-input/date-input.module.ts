import { NgModule } from '@angular/core';
import { DateInputComponent } from './date-input.component';
import { SharedModule } from './../shared.module';
import { FormLabelModule } from '../form-label/form-label.module';
import { FormHelperTextModule } from '../form-helper-text/form-helper-text.module';
import { TooltipModule } from '../tooltip/tooltip.module';


@NgModule({
  declarations: [DateInputComponent],
  imports: [ SharedModule, FormLabelModule, FormHelperTextModule, TooltipModule],
  exports: [DateInputComponent]
})
export class DateInputModule { }
