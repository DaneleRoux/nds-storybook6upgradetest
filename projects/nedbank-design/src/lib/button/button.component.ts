import { Component, Input, Output, ViewEncapsulation, EventEmitter } from '@angular/core';

@Component({
  selector: 'ui-button',
  templateUrl: 'button.component.html' ,
  styleUrls: ['./button.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ButtonComponent {
  
  /**
  * The content of the button.
  */
  @Input() buttonText: string = '';

  /**
  * The content of the button.
  */
  @Input() secondaryButtonText: string = '';

  /**
  * Is it a secondary button?
  */
  @Input() secondary: boolean = false;

  /**
  * Does the button have a transparent background?
  */
  @Input() textButton: boolean = false;

  /**
  * Does the button have an icon?
  */
  @Input() iconbutton: boolean = false;

  /**
  * Is the icon before the text?
  */
  @Input() iconbefore: boolean = false;

  /**
  * Is the icon after the text?
  */
  @Input() iconafter: boolean = false;

  /**
  * The name of the icon.
  */
  @Input() iconName: string = '';

  /**
   * The size of the icon - 16px, 24px or 32px.
   */
  @Input() iconSize: string = '';

  /**
   * The icon's color.
   */
  @Input() iconColor: string = '';

  /**
  * Is the icon a spinner?
  */
  @Input() spinner: boolean = false;

  /**
  * Is the button disabled?
  */
  @Input() disabled: boolean = false;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  /**
  * The eventEmitters to update hide and show values + Which BTN was pressed
  */
  @Output() buttonClickedUpdate = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  buttonClicked() {
    //Set the global button value to true / Clicked
    this.buttonClickedUpdate.emit(true);
  }

  disabledButton(event) {
    const element = event.target;
    if (element.hasAttribute('disabled')) {
      document.querySelector(".nlsg-c-tooltip").classList.add("showTooltip");
    }
  }
}

