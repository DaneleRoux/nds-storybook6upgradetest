import { NgModule } from '@angular/core';
import { SharedModule } from './../shared.module';
import { ButtonComponent } from './button.component';
import { IconsModule } from './../icons/icons.module';

@NgModule({
  declarations: [ButtonComponent],
  imports: [SharedModule, IconsModule],
  exports: [ButtonComponent]
})
export class ButtonModule { }