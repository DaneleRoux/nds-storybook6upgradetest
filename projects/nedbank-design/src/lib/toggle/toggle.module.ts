import { NgModule } from '@angular/core';
import { ToggleComponent } from './toggle.component';
import { SharedModule } from './../shared.module';
import { FormLabelModule } from '../form-label/form-label.module';
import { FormHelperTextModule } from '../form-helper-text/form-helper-text.module';
import { TooltipModule } from '../tooltip/tooltip.module';

@NgModule({
  declarations: [ToggleComponent],
  imports: [SharedModule, FormLabelModule, FormHelperTextModule, TooltipModule],
  exports: [ToggleComponent]
})
export class ToggleModule { }
