import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ui-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss']
})
export class ToggleComponent implements OnInit {

  /**
  * The toggle's label.
  */
//  @Input() toggleLabel: string;

  /**
  * The label's text.
  */
  @Input() labelText: string = "Default text";

  /**
  * The input aria name.
  */
  @Input() arialabel: string;

  /**
   * Does the label have a tooltip?
   */
  @Input() hideTooltip: boolean = true;

  /**
   * Does the tooltip have a heading?
   */
  @Input() hasHeading: boolean = false;

  /**
   * The tooltip's heading.
   */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltip's text.
  */
  @Input() tooltipText: string;

  /**
  * Is the toggle checked by default?
  */
  @Input() checked: boolean = false;

  /**
  * Does input has Helper text.
  */
  @Input() hashelperText: boolean = true;

  /**
  * Helper text 
  */
  @Input() helperText: string = 'Default helper text';

  /**
  * The disabled state the input.
  */
  @Input() disabled: boolean = false;

  /**
   * Valid input.
   */
  @Input() isValid: boolean = false;

  /**
   * Invalid input.
   */
  @Input() notValid: boolean = false;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  /**
   * The toggle checked emitted
  */
   @Output() toggleCheckedUpdate = new EventEmitter();

  constructor() { }

  ngOnInit() {

  }

  toggleChecked(event) {

    this.checked = !this.checked;

    //Emit event and toggle checked
    this.toggleCheckedUpdate.emit({event: event, checked: this.checked});
  }

}
