import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ui-nedbank-design',
  template: `
    <p>
      nedbank-design works!
    </p>
  `,
  styleUrls: ['./nedbank-design.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NedbankDesignComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
