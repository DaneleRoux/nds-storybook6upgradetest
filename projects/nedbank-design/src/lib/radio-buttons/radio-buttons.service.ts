import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


const localUrl = 'mockData.json';

@Injectable({
  providedIn: 'root'
})
export class RadioButtonsService {

  constructor(private http: HttpClient) { }

  getSmartphone() {
    return this.http.get(localUrl);
  }
}
