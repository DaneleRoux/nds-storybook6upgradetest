import { NgModule } from '@angular/core';
import { RadioButtonsComponent } from './radio-buttons.component';
import { SharedModule } from './../shared.module';
import { FormLabelModule } from '../form-label/form-label.module';
import { FormHelperTextModule } from '../form-helper-text/form-helper-text.module';
import { TooltipModule } from '../tooltip/tooltip.module';

@NgModule({
  declarations: [RadioButtonsComponent],
  imports: [SharedModule, FormLabelModule, FormHelperTextModule, TooltipModule],
  exports: [RadioButtonsComponent]
})
export class RadioButtonsModule { }
