import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';

@Component({
  selector: 'ui-radio-buttons',
  templateUrl: './radio-buttons.component.html',
  styleUrls: ['./radio-buttons.component.scss']
})
export class RadioButtonsComponent implements OnInit {

  /**
  * The data set to create the radio button and groups.
  */
  @Input() radiobuttons: any;

  /**
  * Does the label have a tooltip?
  */
  @Input() hideTooltip: boolean = true;

  /**
  * Does the tooltip have a heading?
  */
  @Input() hasHeading: boolean;

  /**
  * The tooltip's heading.
  */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltip's text.
  */
  @Input() tooltipText: string;

  /**
  * The disabled state the input.
  */
  @Input() disabled: boolean = false;

  /**
  * Valid input.
  */
  @Input() isValid: boolean;

  /**
  * Invalid input.
  */
  @Input() notValid: boolean;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  /**
  * Radio button clicked value
  */
  @Output() radioClickedUpdate = new EventEmitter();


  constructor() {}

  ngOnInit() {
  }

  radioButtonValue(radioButtonValue) {
    //Emit radio label string for check
    this.radioClickedUpdate.emit(radioButtonValue);
  }

}
