import { TestBed } from '@angular/core/testing';

import { RadioButtonsService } from './radio-buttons.service';

describe('RadioButtonsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RadioButtonsService = TestBed.get(RadioButtonsService);
    expect(service).toBeTruthy();
  });
});
