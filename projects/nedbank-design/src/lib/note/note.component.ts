import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ui-note',
  templateUrl: 'note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {

  /**
  * Is this an informative note?
  */
  @Input() infoNote: boolean = true;

  /**
   * Is this a warning note?
   */
  @Input() warningNote: boolean = false;

  /**
  * Note heading text.
  */
  @Input() noteHeading: string = '';

  /**
  * Note body text.
  */
  @Input() noteBody: string = '';

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  constructor() { }

  ngOnInit() {
  }

}
