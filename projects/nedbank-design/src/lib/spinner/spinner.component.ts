import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ui-spinner',
  templateUrl: 'spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {

  /**
  * Does the spinner contain an icon?
  */
  @Input() hasIcon: boolean = true;

  /**
  * The name of the icon.
  */
  @Input() iconName: string = '';

  iconColor: string = 'nlsg-icon--colour-grass-green';

  iconSize: string = 'nlsg-icon--size-32';

  /**
  * Is the lock icon white?
  */
  @Input() whiteSpinner: boolean = false;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  constructor() { }

  ngOnInit() {
  }

}
