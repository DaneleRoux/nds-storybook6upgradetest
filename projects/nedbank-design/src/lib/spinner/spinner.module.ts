import { NgModule } from '@angular/core';
import { SpinnerComponent } from './spinner.component';
import { IconsModule } from '../icons/icons.module';
import { SharedModule } from './../shared.module';


@NgModule({
  declarations: [SpinnerComponent],
  imports: [SharedModule, IconsModule],
  exports: [SpinnerComponent]
})
export class SpinnerModule { }
