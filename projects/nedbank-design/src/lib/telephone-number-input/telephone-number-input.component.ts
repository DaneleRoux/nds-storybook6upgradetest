import { Component, OnInit, Input, Output, ViewEncapsulation, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SearchCountryField, CountryISO } from 'ngx-intl-tel-input';

@Component({
  selector: 'ui-telephone-number-input',
  templateUrl: 'telephone-number-input.component.html' ,
  styleUrls: ['./telephone-number-input.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class TelephoneNumberInputComponent implements OnInit {
  inputId: string;
  separateDialCode = true;
	SearchCountryField = SearchCountryField;
	CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [CountryISO.SouthAfrica];
	phoneForm = new FormGroup({
		phone: new FormControl(undefined, [Validators.required])
  });

  /**
  * Input ID
  */
  @Input() inputID: string;

  /**
  * Number value in second input
  */
  numberValue: number;

  /**
  * Does the input have a label?
  */
  @Input() hasLabel: boolean = true;

  /**
  * The label's text.
  */
  @Input() labelText: string;

  /**
   * Does the label have a tooltip?
   */
  @Input() hideTooltip: boolean = true;

  /**
   * Does the tooltip have a heading?
   */
  @Input() hasHeading: boolean = false;

  /**
   * The tooltip's heading.
   */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltip's text.
  */
  @Input() tooltipText: string;

  /**
  * Does input has Helper text.
  */
  @Input() hashelperText: boolean = true;

  /**
  * Helper text 
  */
  @Input() helperText: string = 'Default helper text';

  /**
  * The disabled state of telephone input.
  */
  @Input() disabled: boolean = false;

  /**
  * Valid telephone input.
  */
  @Input() isValid: boolean = false;

  /**
  * Not a valid telephone input.
  */
  @Input() notValid: boolean = false;


  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
   * Margin bottom 30px.
   */
  @Input() marginBottomMd: boolean;

  /**
   * Margin bottom 60px.
   */
  @Input() marginBottomLg: boolean;

  /**
   * Margin bottom 0px.
   */
  @Input() marginBottomNone: boolean;

  /**
  * The value emitted + data about country and dialing code
  */
  @Output() numberValueUpdated = new EventEmitter();

  constructor() {}

  numberDetails(){
    this.numberValueUpdated.emit(this.numberValue);
  }
  

  ngOnInit() {}

  ngAfterViewInit() {}
}
