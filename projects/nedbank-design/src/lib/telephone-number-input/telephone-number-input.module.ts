import { NgModule } from '@angular/core';
import { TelephoneNumberInputComponent } from './telephone-number-input.component';
// import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { SharedModule } from './../shared.module';
import { FormLabelModule } from '../form-label/form-label.module';
import { FormHelperTextModule } from '../form-helper-text/form-helper-text.module';
import { TooltipModule } from '../tooltip/tooltip.module';

@NgModule({
  declarations: [
    TelephoneNumberInputComponent
  ],
  imports: [
    NgxIntlTelInputModule,
    // BsDropdownModule.forRoot(),
    SharedModule, FormLabelModule, FormHelperTextModule, TooltipModule
  ],
  exports: [
    TelephoneNumberInputComponent
  ]
})
export class TelephoneNumberInputModule { }
