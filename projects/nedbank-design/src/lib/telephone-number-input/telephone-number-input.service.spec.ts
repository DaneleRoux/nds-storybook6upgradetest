import { TestBed } from '@angular/core/testing';

import { TelephoneNumberInputService } from './telephone-number-input.service';

describe('TelephoneNumberInputService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TelephoneNumberInputService = TestBed.get(TelephoneNumberInputService);
    expect(service).toBeTruthy();
  });
});
