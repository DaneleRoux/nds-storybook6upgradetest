import { NgModule } from '@angular/core';
import { SharedModule } from './../shared.module';
import { InputComponent } from './input.component';
import { FormLabelModule } from '../form-label/form-label.module';
import { FormHelperTextModule } from '../form-helper-text/form-helper-text.module';
import { TooltipModule } from '../tooltip/tooltip.module';

@NgModule({
  declarations: [InputComponent],
  imports: [
    SharedModule, FormLabelModule, FormHelperTextModule, TooltipModule
  ],
  exports: [InputComponent],
  bootstrap: [InputComponent]
})
export class InputModule { }

