import { Component, OnInit, Input, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'ui-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class InputComponent implements OnInit {

  /**
  * Does the input have a label?
  */
  @Input() hasLabel: boolean = true;

  /**
  * The label's text.
  */
  @Input() labelText: string = 'Default label';
  
  /**
  * The input aria label.
  */
  @Input() arialabel: string;

  /**
   * Does the label have a tooltip?
   */
  @Input() hideTooltip: boolean = true;

  /**
   * Does the tooltip have a heading?
   */
  @Input() hasHeading: boolean;

  /**
   * The tooltip's heading.
   */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltip's text.
  */
  @Input() tooltipText: string;

  /**
  * The input type.
  */
  @Input() type: string;

  /**
  * The input name.
  */
  @Input() name: string;

  /**
  * The input default value.
  */
  @Input() defaultValue: any;

  /**
  * The input's placeholder text.
  */
  @Input() placeholderText: string;

  /**
  * Show/ Hide search icon 
  */
  @Input() searchIcon: boolean = false;

  /**
  * Set input to currency input  
  */
  @Input() currencyInput: boolean = false;

  /**
  * Show currency symbol 
  */
  @Input() currencySymbol: string;

  /**
  * Does input has Helper text.
  */
  @Input() hashelperText: boolean = true;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  /**
  * Helper text 
  */
  @Input() helperText: string = 'Default helper text';

  /**
  * The disabled state the input.
  */
  @Input() disabled: boolean = false;

  /**
  * Valid input.
  */
  @Input() isValid: boolean;

  /**
  * Invalid input.
  */
  @Input() notValid: boolean;



  ngOnInit() {}

  inputChanged(event) {
    const element = event.target;
    let inputValue = event.target.value;

    // if (inputValue) {
    //   element.classList.add('valid');
    // }
    // else {
    //   element.classList.remove('valid');
    // }

    if (element.classList.contains('searchIcon')) {
      const searchIcon = element.nextElementSibling.nextElementSibling;

      if (inputValue) {
        searchIcon.classList.add('nlsg-icon-close');
        searchIcon.classList.remove('nlsg-icon-search');
      }
      else {
        searchIcon.classList.remove('nlsg-icon-close');
        searchIcon.classList.add('nlsg-icon-search');
        inputValue = element.value = '';
      }
    }
  }

  togglePasswordIcon(event) {
    const element = event.target;
    element.classList.toggle('nlsg-icon-hide');

    if (element.previousElementSibling.type === 'password') {
     element.previousElementSibling.type = 'text';
     element.classList.add('nlsg-icon-hide');
     element.style.display = 'block';
     element.classList.remove('nlsg-icon-eye');
    } else {
     element.previousElementSibling.type = 'password';
     element.classList.remove('nlsg-icon-hide');
     element.style.display = 'block';
     element.classList.add('nlsg-icon-eye');
    }
  }

  toggleSearchIcon(event) {
    const element = event.target;
    let inputSearchValue = element.previousElementSibling.previousElementSibling.value;

    if (inputSearchValue) {
      inputSearchValue = element.previousElementSibling.previousElementSibling.value = '';
      element.classList.remove('nlsg-icon-close');
      element.classList.add('nlsg-icon-search');
      element.previousElementSibling.previousElementSibling.classList.remove('valid');
    }
  }
}
