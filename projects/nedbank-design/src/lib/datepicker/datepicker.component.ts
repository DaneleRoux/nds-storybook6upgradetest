import { Component, ElementRef, Input, Output, EventEmitter, OnChanges, ViewEncapsulation} from '@angular/core';
import { IAngularMyDpOptions, IMyRangeDateSelection, IMyDateModel, IMyCalendarViewChanged, IMyInputFieldChanged } from 'angular-mydatepicker';

@Component({
  selector: 'ui-datepicker',
  templateUrl: 'datepicker.component.html' ,
  styleUrls: ['./datepicker.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DatepickerComponent implements OnChanges {

  /**
  * Does the input have a label?
  */
  @Input() hasLabel: boolean = true;

  /**
  * The label's text.
  */
  @Input() labelText: string;

  /**
  * The input aria name.
  */
  @Input() arialabel: string;

  /**
  * Does the label have a tooltip?
  */
  @Input() hideTooltip: boolean  = true;

  /**
   * Does the tooltip have a heading?
   */
  @Input() hasHeading: boolean  = false;

  /**
   * The tooltip's heading.
   */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltip's text.
  */
  @Input() tooltipText: string;

  /**
  * The datepicker's placeholder text.
  */
  @Input() placeholder: string;

  /**
  * The range/single options.
  */
  @Input() rangeOn: boolean  = false;

  /**
   * Datepicker initialize with default date
   */
  @Input() myDateInit: boolean;

  /**
   * Datepicker initialize with single date
   */
  @Input() singleDefault: any;

  /**
  * The disableDates option.
  */
  @Input() disableDates: any;

  /**
   * Datepicker initialize with start range date
   */
  @Input() rangeBeginDefault: any;

  /**
   * Datepicker initialize with end range date
   */
  @Input() rangeEndDefault: any;

  /**
  * Does input have Helper text?
  */
  @Input() hashelperText: boolean = true;

  /**
   * Helper text.
   */
  @Input() helperText: string = 'Default helper text';

  /**
  * The disabled state the input.
  */
  @Input() disabled: boolean  = false;

  /**
  * Valid input.
  */
  @Input() isValid: boolean  = false;

  /**
  * Invalid input.
  */
  @Input() notValid: boolean  = false;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  /**
  * Current date selected
  */
  @Output() dateUpdated = new EventEmitter();

  // Datepicker options
  myDpOptions: IAngularMyDpOptions = {
    dateRange: true,
    dateFormat: 'dd/mm/yyyy',
    sunHighlight: false,
    showMonthNumber: false,
    closeSelectorOnDateSelect: true,
    // disableDates:  this.disabledDates,
    firstDayOfWeek: 'su',
    dayLabels: {su: 'S', mo: 'M', tu: 'T', we: 'W', th: 'T', fr: 'F', sa: 'S'},
    monthLabels: {
      1: 'January', 2: 'February',
      3: 'March', 4: 'April', 5: 'May', 6: 'June',
      7: 'July', 8: 'August', 9: 'September', 10: 'October',
      11: 'November', 12: 'December' }
  };

  model: IMyDateModel = null;

  

  constructor(private el: ElementRef) {}

  ngOnChanges() {
    this.myDpOptions.dateRange = this.rangeOn;
    this.myDpOptions.disableDates = this.disableDates;

    // Is this datepicker initialize with a default date?
    if (this.myDateInit) {

      // Initialize to specific date range with IMyDate object.
      if (this.rangeOn === true) {
        this.model = {
          isRange: true,
          singleDate: null,
          dateRange: {
            beginDate: {
              year: this.rangeBeginDefault.year,
              month: this.rangeBeginDefault.month,
              day: this.rangeBeginDefault.day
            },
            endDate: {
              year: this.rangeEndDefault.year,
              month: this.rangeEndDefault.month,
              day: this.rangeEndDefault.day
            }
          }
        };
        // Initialize to specific single date with IMyDate object
      } else {
        this.model = {isRange: false, singleDate: {
        date :
          { year: this.singleDefault.year,
            month: this.singleDefault.month,
            day: this.singleDefault.day
          }
        }};
      }
    }
  }

  // Selecting a range
  onRangeDateSelection(rangeClicked: IMyRangeDateSelection): void {
    const item = event.target as HTMLElement;
    setTimeout(() => {

      // Selecting first in range
      if (rangeClicked.isBegin === true ) {

        // Reset classes
        const reset = this.el.nativeElement.querySelectorAll('.myDpDaycell');
        for (let i = 0; i < reset.length; i++) {
          reset[i].classList.remove('end-of-range');
          reset[i].classList.remove('edge-forward-none');
        }

        // Check if at the end of calender
        if (item.parentElement.classList.contains('end-of-calender')) {
          item.parentElement.classList.add('edge-forward-none');
        }
      }
      // Selecting second in range
      else {
        // Check if at the start of calender
        if (item.parentElement.classList.contains('start-of-calender')) {
          item.parentElement.classList.remove('end-of-range');
          item.parentElement.classList.add('edge-forward-none');
        }

        // Else add end-of-range class
        item.parentElement.classList.add('end-of-range');
      }
    }, 10);
  }

  // Check if dates are at the start or end
  RoundedCorners() {
    setTimeout(() => {
    // Round the dates on the edges of the calender
    const edges = document.body.querySelectorAll('.myDpDaycell');
    [].forEach.call(edges, (node, i) => {
      if (i === 6 || i === 13 || i === 20 || i === 27 || i === 34) {
        node.classList.add('end-of-calender');
      }
      else if (i === 0 || i === 7 || i === 14 || i === 21 || i === 28 ) {
        node.classList.add('start-of-calender');
      }
    });
    }, 10);
  }

  // Check if the input has a value and add valid class
  onInputFieldChanged(event: IMyInputFieldChanged) {
    if (event.value) {
      const thisInput = this.el.nativeElement.querySelector('.input-box') as HTMLElement;
      // thisInput.classList.add('vaild');
      this.dateUpdated.emit(event.value);
    }
  }

  // Check when calender opens
  onCalendarToggle(event: number): void {

    const toggleBtn = this.el.nativeElement.querySelector('.calender-icon');

    toggleBtn.setAttribute('aria-expanded', 'true');

    if (event === 1) {
      // First round ends
      this.RoundedCorners();

      // Reset classes
      const reset = this.el.nativeElement.querySelectorAll('.myDpDaycell');
      for (let i = 0; i < reset.length; i++) {
        reset.classList.remove('end-of-range');
      }

      setTimeout(() => {
        // Loop through selections
        const allSelections = this.el.nativeElement.querySelectorAll('.myDpSelectedDay');
        for (let i = 0; i < allSelections.length; i++) {

          // Check first selection for the end-of-calender
          if (allSelections[0].classList.contains('end-of-calender')) {
            allSelections[0].classList.add('edge-forward-none');
          }
          // Check last selection for the start-of-calender
          if (allSelections[allSelections.length - 1].classList.contains('start-of-calender')) {
            allSelections[allSelections.length - 1].classList.add('edge-forward-none');
          }

          // Else add end-of-range class
          allSelections[allSelections.length - 1].classList.add('end-of-range');
        }

        // Check if there is only one selection in calender view at a time
        if (allSelections.length === 1) {
          allSelections[0].classList.add('start-of-range');
        }

        // Add focus class to both input and calender
        const thisInput = this.el.nativeElement.querySelector('.input-box') as HTMLElement;
        thisInput.classList.add('focus');
        const calenderDropdown = this.el.nativeElement.querySelector('.myDpSelector') as HTMLElement;
        calenderDropdown.classList.add('focus');

        }, 10);
    }
    else {

      toggleBtn.setAttribute('aria-expanded', 'false');

      // Remove focus on close of calender
      const thisInput = this.el.nativeElement.querySelector('.input-box') as HTMLElement;
      thisInput.classList.remove('focus');
    }
  }

  onCalendarViewChanged(event: IMyCalendarViewChanged) {

    // First round ends
    this.RoundedCorners();

    setTimeout(() => {
      // Get all selected days
      const allSelections = document.querySelectorAll('.myDpSelectedDay');
      for (let i = 0; i < allSelections.length; i++) {
        // Add end-of-range class to last selection
        allSelections[allSelections.length - 1].classList.add('end-of-range');

        // Check if there is only 1 selection in view and the next day has a range meaning its the start of range
        if (allSelections.length === 1 && allSelections[i].nextElementSibling.classList.contains('myDpRangeColor')) {
          allSelections[0].classList.add('start-of-range');
        }
      }
    }, 10);
  }



}
