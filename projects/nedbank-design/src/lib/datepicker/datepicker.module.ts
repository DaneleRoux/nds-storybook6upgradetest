import { NgModule } from '@angular/core';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { DatepickerComponent } from './datepicker.component';
import { SharedModule } from './../shared.module';
import { FormLabelModule } from '../form-label/form-label.module';
import { FormHelperTextModule } from '../form-helper-text/form-helper-text.module';
import { TooltipModule } from '../tooltip/tooltip.module';

@NgModule({
  declarations: [DatepickerComponent],
  imports: [
    AngularMyDatePickerModule, SharedModule, FormLabelModule, FormHelperTextModule, TooltipModule
  ],
  exports: [
    DatepickerComponent]
})
export class DatepickerModule { }
