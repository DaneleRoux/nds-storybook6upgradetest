import { NgModule } from '@angular/core';
import { SharedModule } from './../shared.module';
import { AccordionComponent } from './accordion.component';


@NgModule({
  declarations: [AccordionComponent],
  imports: [
    SharedModule
  ],
  exports: [AccordionComponent]
})
export class AccordionModule { }
