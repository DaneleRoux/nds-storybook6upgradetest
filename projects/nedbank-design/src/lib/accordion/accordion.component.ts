import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ui-accordion',
  templateUrl: 'accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {

  constructor() {}

  /**
  * Condensed accordion, used inside a container.
  */
  @Input() smallAccordion: boolean = false;

  /**
  * Should the 1st accordion be active and it's content be open?
  */
  @Input() openFirstAccorion: boolean = true;

  /**
  * The accordion's data set.
  */
  @Input() accordionData:  any;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  ngOnInit() {
    if (!this.accordionData) {
      throw new TypeError("'accordionData' is a required property");
    }
  }

  toggleAccordian(event) {
    const trigger = event.target;
    const allTriggers = document.querySelectorAll('.accordion-trigger');
    const allPanels = document.querySelectorAll('.nlsg-c-accordion--content');
    const panel = trigger.nextElementSibling;
    
    if(trigger.classList.contains('active')) {
      // Do nothing
      trigger.setAttribute('aria-expanded', 'true');
    }
    else {
      [].forEach.call(allTriggers, (triggerEl) => {
        triggerEl.classList.remove('active');
        triggerEl.setAttribute('aria-expanded', 'false');
      });
      trigger.classList.add('active');
      trigger.setAttribute('aria-expanded', 'true');
      [].forEach.call(allPanels, (panelEl) => {
        panelEl.style.maxHeight = null;
        panelEl.classList.remove('open');
      });
      panel.style.maxHeight = panel.scrollHeight + 'px';
    }

  }

}
