import { TestBed } from '@angular/core/testing';

import { PaginationTestService } from './pagination-test.service';

describe('PaginationTestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaginationTestService = TestBed.get(PaginationTestService);
    expect(service).toBeTruthy();
  });
});
