import { NgModule } from '@angular/core';
import { PaginationTestComponent } from './pagination-test.component';
import { SharedModule } from './../shared.module';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [PaginationTestComponent],
  imports: [
    NgxPaginationModule, SharedModule
  ],
  exports: [PaginationTestComponent]
})
export class PaginationTestModule { }
