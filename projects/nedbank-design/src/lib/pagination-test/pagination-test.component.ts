import { Component, Input } from '@angular/core';

@Component({
  selector: 'lib-pagination-test',
  templateUrl: 'pagination.test.component.html' ,
  styleUrls: ['./pagination.test.component.scss'],
})
export class PaginationTestComponent {
  
  @Input() paginationSettings: any;
  
  // Some dummy data
  mockData = [
    {content: 'Item 1'},
    {content: 'Item 2'},
    {content: 'Item 3'},
    {content: 'Item 4'},
    {content: 'Item 5'},
    {content: 'Item 6'},
    {content: 'Item 7'},
    {content: 'Item 8'},
    {content: 'Item 9'},
    {content: 'Item 10'},
    {content: 'Item 11'},
    {content: 'Item 12'},
    {content: 'Item 13'},
    {content: 'Item 14'},
    {content: 'Item 15'},
    {content: 'Item 16'},
    {content: 'Item 17'},
    {content: 'Item 18'},
    {content: 'Item 19'},
    {content: 'Item 20'},
    {content: 'Item 21'},
    {content: 'Item 22'},
    {content: 'Item 23'},
    {content: 'Item 24'},
    {content: 'Item 25'},
    {content: 'Item 26'},
    {content: 'Item 27'},
    {content: 'Item 28'},
    {content: 'Item 29'},
    {content: 'Item 30'},
    {content: 'Item 31'},
    {content: 'Item 32'},
    {content: 'Item 33'},
    {content: 'Item 34'},
    {content: 'Item 35'},
    {content: 'Item 36'},
    {content: 'Item 37'},
    {content: 'Item 38'},
    {content: 'Item 39'},
    {content: 'Item 40'},
  ];

  constructor() { 
  } // constructor ends

}
