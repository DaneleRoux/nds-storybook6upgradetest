import { Component, OnInit, ViewEncapsulation, Input, ViewChild} from '@angular/core';

@Component({
  selector: 'ui-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MultiSelectComponent implements OnInit {

/**
* Does the input have a label?`
*/
@Input() hasLabel: boolean = true;

/**
 * The label's text.
 */
@Input() labelText: string;

/**
* Does the label have a tooltip?
*/
@Input() hideTooltip: boolean = true;

/**
* Does the tooltip have a heading?
*/
@Input() hasHeading: boolean;

/**
* The tooltip's heading.
*/
@Input() tooltipHeadingText: string;

/**
 * The tooltip's text.
 */
@Input() tooltipText: string;

/**
 * The disabled state of telephone input.
 */
@Input() disabled: boolean = false;

/**
 * Valid telephone input.
 */
@Input() isValid: boolean = false;

/**
 * Not a valid telephone input.
 */
@Input() notValid: boolean = false;

/**
 * Does input has Helper text.
 */
@Input() hashelperText: boolean = true;

/**
* Helper text 
*/
@Input() helperText: string = 'Default helper text';

  constructor() { }

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};




  
  ngOnInit() {

    this.dropdownList = [
      { item_id: 1, item_text: 'Apples' },
      { item_id: 2, item_text: 'Oranges' },
      { item_id: 3, item_text: 'Bananas' },
      { item_id: 4, item_text: 'Cherries' },
      { item_id: 5, item_text: 'Coconuts' },
      { item_id: 6, item_text: 'Some long shit for testing' }
    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select all',
      unSelectAllText: 'Select none',
      itemsShowLimit: 3,
      allowSearchFilter: false
    };

  }

  onItemSelect(item: any) {

  }
  onSelectAll(items: any) {

  }

}
