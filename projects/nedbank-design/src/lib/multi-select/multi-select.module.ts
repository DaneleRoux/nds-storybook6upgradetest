import { NgModule } from '@angular/core';
import { MultiSelectComponent } from './multi-select.component';
import { SharedModule } from './../shared.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [MultiSelectComponent],
  imports: [
    NgMultiSelectDropDownModule,
    SharedModule,
  ],
  exports: [MultiSelectComponent]
})
export class MultiSelectModule { }
