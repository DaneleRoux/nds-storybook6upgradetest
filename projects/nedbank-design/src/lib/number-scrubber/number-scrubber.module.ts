import { NgModule } from '@angular/core';
import { NumberScrubberComponent } from './number-scrubber.component';
import { SharedModule } from './../shared.module';
import { FormLabelModule } from '../form-label/form-label.module';
import { TooltipModule } from '../tooltip/tooltip.module';

@NgModule({
  declarations: [NumberScrubberComponent],
  imports: [SharedModule, FormLabelModule, TooltipModule
  ],
  exports: [NumberScrubberComponent]
})
export class NumberScrubberModule { }
