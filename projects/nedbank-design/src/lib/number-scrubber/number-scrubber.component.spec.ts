import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberScrubberComponent } from './number-scrubber.component';

describe('NumberScrubberComponent', () => {
  let component: NumberScrubberComponent;
  let fixture: ComponentFixture<NumberScrubberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumberScrubberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberScrubberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
