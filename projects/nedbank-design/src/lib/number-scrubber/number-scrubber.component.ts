import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'ui-number-scrubber',
  templateUrl: 'number-scrubber.component.html',
  styleUrls: ['./number-scrubber.component.scss']
})
export class NumberScrubberComponent implements OnInit {

  /**
  * Does the input have a label?
  */
  @Input() hasLabel: boolean = true;

  /**
  * The label's text.
  */
 @Input() labelText: string = 'Default label';

  /**
   * The input aria name.
   */
  @Input() arialabel: string;

  /**
   * Does the label have a tooltip?
   */
  @Input() hideTooltip: boolean = true;

  /**
   * Does the tooltip have a heading?
   */
  @Input() hasHeading: boolean;

  /**
   * The tooltip's heading.
   */
  @Input() tooltipHeadingText: string;

  /**
  * The tooltip's text.
  */
  @Input() tooltipText: string;

  /**
  * The maximum vlaue that can be entered.
  */
  @Input() maxValue: number; 
  /**
  * The number scrubber's step increment amount.
  */
  @Input() incrementValue: number = 1;

  /**
  * The number scrubber's value.
  */
  @Input() numbervalue: number = 0;

  /**
  *  Margin bottom 15px. 
  */
  @Input() marginBottomSm: boolean;

  /**
  * Margin bottom 30px.
  */
  @Input() marginBottomMd: boolean;

  /**
  * Margin bottom 60px.
  */
  @Input() marginBottomLg: boolean;

  /**
  * Margin bottom 0px.
  */
  @Input() marginBottomNone: boolean;

  /**
  * The number scrubber's value emitted
  */
  @Output() valueUpdated = new EventEmitter();


  constructor() { 
  }

  ngOnInit() {
  }
  increaseValue(ev) {
    if(this.numbervalue < this.maxValue ) {
      this.numbervalue = (this.numbervalue-0) + (this.incrementValue-0);
    }
    if(this.numbervalue > this.maxValue) {
      this.numbervalue = this.maxValue;
    }
    this.valueUpdated.emit(this.numbervalue);
  }
  decreaseValue() {
    if(this.numbervalue > 0 ) {
      this.numbervalue = (this.numbervalue-0) - (this.incrementValue-0);
    }
    if(this.numbervalue < 0) {
      this.numbervalue = 0;
    }
    this.valueUpdated.emit(this.numbervalue);
  }
}