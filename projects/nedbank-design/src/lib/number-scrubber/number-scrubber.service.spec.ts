import { TestBed } from '@angular/core/testing';

import { NumberScrubberService } from './number-scrubber.service';

describe('NumberScrubberService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NumberScrubberService = TestBed.get(NumberScrubberService);
    expect(service).toBeTruthy();
  });
});
