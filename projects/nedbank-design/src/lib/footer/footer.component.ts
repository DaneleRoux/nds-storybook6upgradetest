import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ui-footer',
  templateUrl: 'footer.component.html' ,
  styleUrls: ['./footer.component.scss'],
  providers: [DatePipe],
  encapsulation: ViewEncapsulation.None
  
})
export class FooterComponent implements OnInit {

  /**
  * The footer's links data set.
  */
  @Input() footerLinks: any;

  /**
  * The data link's URL.
  */
  @Input() linkUrl: any;

  /**
  * The data link's text.
  */
  @Input() linkText: any;

  constructor() { }

  ngOnInit() {
  }

}
