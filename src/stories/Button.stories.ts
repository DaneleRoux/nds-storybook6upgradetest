import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { ButtonComponent } from '../../projects/nedbank-design/src/lib/button/button.component';
import { IconsComponent } from '../../projects/nedbank-design/src/lib/icons/icons.component';

export default {
  title: 'Buttons',
  component: ButtonComponent,
  subcomponents: { IconsComponent },

} as Meta;

const Template: Story<ButtonComponent> = (args: ButtonComponent) => ({
  component: ButtonComponent,
  props: args,
  moduleMetadata: {
    declarations: [ButtonComponent, IconsComponent],
  },
});

// Primary button
export const Primary = Template.bind({});
Primary.args = {
  buttonText: 'Lorem ipsum',
  disabled: false,
};

Primary.argTypes = {

  // Button text
  buttonText: {
    name: 'buttonText',
    description: 'Text to be used in the button.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Button text ends

  // Disabled state
  disabled: {
    name: 'disabled',
    description: 'Is the button disabled?',
    table: {
      category: 'States',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Disabled state ends

}// Primary button ends


// Secondary button
export const Secondary = Template.bind({});
Secondary.args = {
  secondary: true,
  buttonText: 'Lorem ipsum',
  disabled: false,
};

Secondary.argTypes = {

  // Secondary button selection
  secondary: {
    name: 'secondary',
    description: 'Is this a secondary button?',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Secondary button selection ends

  // Button text
  buttonText: {
    name: 'buttonText',
    description: 'Text to be used in the button.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Button text ends

  // Disabled state
  disabled: {
    name: 'disabled',
    description: 'Is the button disabled?',
    table: {
      category: 'States',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Disabled state ends

}// Secondary button ends


// Tertiary button
export const Tertiary = Template.bind({});
Tertiary.args = {
  textButton: true,
  buttonText: 'Lorem ipsum',
  iconbefore: false,
  iconafter: false,
  disabled: false,
};

Tertiary.argTypes = {

  // Secondary button selection
  textButton: {
    name: 'textButton',
    description: 'Is this a tertiary button?',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Secondary button selection ends

  // Button text
  buttonText: {
    name: 'buttonText',
    description: 'Text to be used in the button.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Button text ends

  // Icon before selection
  iconbefore: {
    name: 'iconbefore',
    description: 'Does the button text have an icon in front of it?',
    table: {
      category: 'Icons',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Icon before selection ends

  // Icon after selection
  iconafter: {
    name: 'iconafter',
    description: 'Does the button text have an icon at the end of it?',
    table: {
      category: 'Icons',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Icon after selection ends

  // Icon name selection
  iconName: {
    name: 'iconName',
    description: 'What icon to use before or after the button text.',
    table: {
      category: 'Icons',
      defaultValue: {
        summary: 'string',
      },
      type: {
        summary: 'string',
      },
    },
    control: { type: "select", 
      options: [
        "nlsg-icon-tailarrow-up", 
        "nlsg-icon-tailarrow-down",
        "nlsg-icon-tailarrow-right", 
        "nlsg-icon-tailarrow-left"
      ]},//Icon name select control ends
  },// Icon name selection ends

  // Disabled state
  disabled: {
    name: 'disabled',
    description: 'Is the button disabled?',
    table: {
      category: 'States',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Disabled state ends

}// Tertiary button ends