import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { InlineAlertComponent } from '../../projects/nedbank-design/src/lib/inline-alert/inline-alert.component';

export default {
  title: 'Inline alerts',
  component: InlineAlertComponent,
} as Meta;

const Template: Story<InlineAlertComponent> = (args: InlineAlertComponent) => ({
  component: InlineAlertComponent,
  props: args,
  moduleMetadata: {
    declarations: [InlineAlertComponent],
  },
});

// Informative alert
export const Informative = Template.bind({});
Informative.args = {
  infoInlineAlert: true,
  showAlert: true,
  alertBody: 'Harum voluptatibus at quisquam placeat eum veritatis unde neque perferendis.',
};

Informative.argTypes = {

  // Inline alert type selection
  infoInlineAlert: {
    name: 'infoInlineAlert',
    description: 'Is this an informative inline alert?',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Block alert type selection ends

  // Visible selection
  showAlert: {
    name: 'showAlert',
    description: 'Used to show the alert in the viewport.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Visible selection ends

  // Body text
  alertBody: {
    name: 'alertBody',
    description: 'Text to be used in the alert body.',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Body text ends

}// Informative alert ends


// Success alert
export const Success = Template.bind({});
Success.args = {
  successInlineAlert: true,
  showAlert: true,
  alertBody: 'Harum voluptatibus at quisquam placeat eum veritatis unde neque perferendis.',
};

Success.argTypes = {

  // Inline alert type selection
  successInlineAlert: {
    name: 'successInlineAlert',
    description: 'Is this a success inline alert?',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Block alert type selection ends

  // Visible selection
  showAlert: {
    name: 'showAlert',
    description: 'Used to show the alert in the viewport.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Visible selection ends

  // Body text
  alertBody: {
    name: 'alertBody',
    description: 'Text to be used in the alert body.',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Body text ends

}// Success alert ends


// Warning alert
export const Warning = Template.bind({});
Warning.args = {
  warningInlineAlert: true,
  showAlert: true,
  alertBody: 'Harum voluptatibus at quisquam placeat eum veritatis unde neque perferendis.',
};

Warning.argTypes = {

  // Inline alert type selection
  warningInlineAlert: {
    name: 'warningInlineAlert',
    description: 'Is this warning inline alert?',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Block alert type selection ends

  // Visible selection
  showAlert: {
    name: 'showAlert',
    description: 'Used to show the alert in the viewport.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Visible selection ends

  // Body text
  alertBody: {
    name: 'alertBody',
    description: 'Text to be used in the alert body.',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Body text ends

}// Warning alert ends


// Error alert
export const Error = Template.bind({});
Error.args = {
  errorInlineAlert: true,
  showAlert: true,
  alertBody: 'Harum voluptatibus at quisquam placeat eum veritatis unde neque perferendis.',
};

Error.argTypes = {

  // Inline alert type selection
  errorInlineAlert: {
    name: 'errorInlineAlert',
    description: 'Is this an error inline alert?',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Block alert type selection ends

  // Visible selection
  showAlert: {
    name: 'showAlert',
    description: 'Used to show the alert in the viewport.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Visible selection ends

  // Body text
  alertBody: {
    name: 'alertBody',
    description: 'Text to be used in the alert body.',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Body text ends

}// Error alert ends


// storiesOf('Alerts - inline', module)
  
//   .addDecorator(centered)
  
//   .addDecorator(
//     moduleMetadata({
//       declarations: [InlineAlertComponent]
//     })
//   )
//   .add(
//     'Informative alert', () => ({
//       template: `
//         <ui-inline-alert 
//           [infoInlineAlert]="true" 
//           [showAlert]="true" 
//           alertBody="Aliquid aperiam laboriosam pariatur." 
//         ></ui-inline-alert>
//       `,
//       props: {
//         // alertBody: text('Alert body', 'Aliquid aperiam laboriosam pariatur.', 'General'),
//         // infoInlineAlert: true
//       }
//     }),
//     {
//       'in-dsm': {
//         id: '5e95ac63668c796d7b93f6cf',
//         componentPath: '../../projects/nedbank-design/src/lib/inline-alert/inline-alert.component'
//       }
//     },
//   )

//   .add(
//     'Warning alert', () => ({
//       template: `
//         <ui-inline-alert 
//           [warningInlineAlert]="true" 
//           [showAlert]="true" 
//           alertBody="Aliquid aperiam laboriosam pariatur." 
//         ></ui-inline-alert>
//       `,
//       props: {
//         // alertBody: text('Alert body', 'Please try again.', 'General'),
//         // warningInlineAlert: true
//       }
//     }),
//     {
//       'in-dsm': {
//         id: '5e96e5e583d247170c937e03',
//         componentPath: '../../projects/nedbank-design/src/lib/inline-alert/inline-alert.component'
//       }
//     },
//   )

//   .add(
//     'Success alert', () => ({
//       template: `
//         <ui-inline-alert 
//           [successInlineAlert]="true" 
//           [showAlert]="true" 
//           alertBody="Aliquid aperiam laboriosam pariatur." 
//         ></ui-inline-alert>
//       `,
//       props: {
//         // alertBody: text('Alert body', 'Aliquid aperiam laboriosam pariatur.', 'General'),
//         // successInlineAlert: true
//       }
//     }),
//     {
//       'in-dsm': {
//         id: '5e96e5da228e9f539c08bf10',
//         componentPath: '../../projects/nedbank-design/src/lib/inline-alert/inline-alert.component'
//       }
//     },
//   )

//   .add(
//     'Error alert', () => ({
//       template: `
//         <ui-inline-alert 
//           [errorInlineAlert]="true" 
//           [showAlert]="true" 
//           alertBody="Aliquid aperiam laboriosam pariatur." 
//         ></ui-inline-alert>
//       `,
//       props: {
//         // alertBody: text('Alert body', 'Aliquid aperiam laboriosam pariatur.', 'General'),
//         // errorInlineAlert: true
//       }
//     }),
//     {
//       'in-dsm': {
//         id: '5e96e5fb83d2473c4c937e05',
//         componentPath: '../../projects/nedbank-design/src/lib/inline-alert/inline-alert.component'
//       }
//     },
//   )
