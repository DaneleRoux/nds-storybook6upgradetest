import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { BlockAlertComponent } from '../../projects/nedbank-design/src/lib/block-alert/block-alert.component';

export default {
  title: 'Block alerts',
  component: BlockAlertComponent,
} as Meta;

const Template: Story<BlockAlertComponent> = (args: BlockAlertComponent) => ({
  component: BlockAlertComponent,
  props: args,
  moduleMetadata: {
    declarations: [BlockAlertComponent],
  },
});

// Informative alert
export const Informative = Template.bind({});
Informative.args = {
  infoBlockAlert: true,
  fixedAlert: false,
  alertVisible: true,
  singleLineBlockAlert: false,
  alertHeading: 'Lorem ipsum dolor sit',
  alertBody: 'Harum voluptatibus at quisquam placeat eum veritatis unde neque perferendis.',
  hasLinks: true,
  link1Text: 'Primary action',
  link1URL: 'http://www.google.com',
  link2Text: 'Secondary action',
  link2URL: 'http://www.google.com'
};

Informative.argTypes = {

  // Block alert type selection
  infoBlockAlert: {
    name: 'infoBlockAlert',
    description: 'Is this an informative block alert?',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Block alert type selection ends

  // Fixed selection
  fixedAlert: {
    name: 'fixedAlert',
    description: 'Whether the alert is fixed in the viewport.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Fixed selection ends

  // Visible selection
  alertVisible: {
    name: 'alertVisible',
    description: 'Used to show the alert in the viewport.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Visible selection ends

  // Single line selection
  singleLineBlockAlert: {
    name: 'singleLineBlockAlert',
    description: 'Does the alert only have 1 line of text (ie. no body text)?',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Single line ends

  // Heading text
  alertHeading: {
    name: 'alertHeading',
    description: 'Text to be used in the alert heading.',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Heading text ends

  // Body text
  alertBody: {
    name: 'alertBody',
    description: 'Text to be used in the alert body.',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Body text ends

  // Has links selection
  hasLinks: {
    name: 'hasLinks',
    description: 'Does the alert have links?',
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Has links ends

  // Link 1 text
  link1Text: {
    name: 'link1Text',
    description: "The alert's first link's text content.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

  // Link 1 text
  link1URL: {
    name: 'link1URL',
    description: "The alert's first link's URL.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

  // Link 1 text
  link2Text: {
    name: 'link2Text',
    description: "The alert's second link's text content.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

  // Link 1 text
  link2URL: {
    name: 'link2URL',
    description: "The alert's second link's URL.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends
  
}// Informative alert ends

// Success alert
export const Success = Template.bind({});
Success.args = {
  successBlockAlert: true,
  fixedAlert: false,
  alertVisible: true,
  singleLineBlockAlert: false,
  alertHeading: 'Lorem ipsum dolor sit',
  alertBody: 'Harum voluptatibus at quisquam placeat eum veritatis unde neque perferendis.',
  hasLinks: true,
  link1Text: 'Primary action',
  link1URL: 'http://www.google.com',
  link2Text: 'Secondary action',
  link2URL: 'http://www.google.com'
};

Success.argTypes = {

  // Block alert type selection
  successBlockAlert: {
    name: 'successBlockAlert',
    description: 'Is this a success block alert?',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Block alert type selection ends

  // Fixed selection
  fixedAlert: {
    name: 'fixedAlert',
    description: 'Whether the alert is fixed in the viewport.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Fixed selection ends

  // Visible selection
  alertVisible: {
    name: 'alertVisible',
    description: 'Used to show the alert in the viewport.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Visible selection ends

  // Single line selection
  singleLineBlockAlert: {
    name: 'singleLineBlockAlert',
    description: 'Does the alert only have 1 line of text (ie. no body text)?',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Single line ends

  // Heading text
  alertHeading: {
    name: 'alertHeading',
    description: 'Text to be used in the alert heading.',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Heading text ends

  // Body text
  alertBody: {
    name: 'alertBody',
    description: 'Text to be used in the alert body.',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Body text ends

  // Has links selection
  hasLinks: {
    name: 'hasLinks',
    description: 'Does the alert have links?',
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Has links ends

  // Link 1 text
  link1Text: {
    name: 'link1Text',
    description: "The alert's first link's text content.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

  // Link 1 text
  link1URL: {
    name: 'link1URL',
    description: "The alert's first link's URL.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

  // Link 1 text
  link2Text: {
    name: 'link2Text',
    description: "The alert's second link's text content.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

  // Link 1 text
  link2URL: {
    name: 'link2URL',
    description: "The alert's second link's URL.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

}// Success alert ends


// Warning alert
export const Warning = Template.bind({});
Warning.args = {
  warningBlockAlert: true,
  fixedAlert: false,
  alertVisible: true,
  singleLineBlockAlert: false,
  alertHeading: 'Lorem ipsum dolor sit',
  alertBody: 'Harum voluptatibus at quisquam placeat eum veritatis unde neque perferendis.',
  hasLinks: true,
  link1Text: 'Primary action',
  link1URL: 'http://www.google.com',
  link2Text: 'Secondary action',
  link2URL: 'http://www.google.com'
};

Warning.argTypes = {

  // Block alert type selection
  warningBlockAlert: {
    name: 'warningBlockAlert',
    description: 'Is this a warning block alert?',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Block alert type selection ends

  // Fixed selection
  fixedAlert: {
    name: 'fixedAlert',
    description: 'Whether the alert is fixed in the viewport.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Fixed selection ends

  // Visible selection
  alertVisible: {
    name: 'alertVisible',
    description: 'Used to show the alert in the viewport.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Visible selection ends

  // Single line selection
  singleLineBlockAlert: {
    name: 'singleLineBlockAlert',
    description: 'Does the alert only have 1 line of text (ie. no body text)?',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Single line ends

  // Heading text
  alertHeading: {
    name: 'alertHeading',
    description: 'Text to be used in the alert heading.',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Heading text ends

  // Body text
  alertBody: {
    name: 'alertBody',
    description: 'Text to be used in the alert body.',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Body text ends

  // Has links selection
  hasLinks: {
    name: 'hasLinks',
    description: 'Does the alert have links?',
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Has links ends

  // Link 1 text
  link1Text: {
    name: 'link1Text',
    description: "The alert's first link's text content.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

  // Link 1 text
  link1URL: {
    name: 'link1URL',
    description: "The alert's first link's URL.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

  // Link 1 text
  link2Text: {
    name: 'link2Text',
    description: "The alert's second link's text content.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

  // Link 1 text
  link2URL: {
    name: 'link2URL',
    description: "The alert's second link's URL.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends
  
}// Warning alert ends


// Error alert
export const Error = Template.bind({});
Error.args = {
  errorBlockAlert: true,
  fixedAlert: false,
  alertVisible: true,
  singleLineBlockAlert: false,
  alertHeading: 'Lorem ipsum dolor sit',
  alertBody: 'Harum voluptatibus at quisquam placeat eum veritatis unde neque perferendis.',
  hasLinks: true,
  link1Text: 'Primary action',
  link1URL: 'http://www.google.com',
  link2Text: 'Secondary action',
  link2URL: 'http://www.google.com'
};

Error.argTypes = {

  // Block alert type selection
  errorBlockAlert: {
    name: 'errorBlockAlert',
    description: 'Is this an error block alert?',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Block alert type selection ends

  // Fixed selection
  fixedAlert: {
    name: 'fixedAlert',
    description: 'Whether the alert is fixed in the viewport.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Fixed selection ends

  // Visible selection
  alertVisible: {
    name: 'alertVisible',
    description: 'Used to show the alert in the viewport.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Visible selection ends

  // Single line selection
  singleLineBlockAlert: {
    name: 'singleLineBlockAlert',
    description: 'Does the alert only have 1 line of text (ie. no body text)?',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Single line ends

  // Heading text
  alertHeading: {
    name: 'alertHeading',
    description: 'Text to be used in the alert heading.',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Heading text ends

  // Body text
  alertBody: {
    name: 'alertBody',
    description: 'Text to be used in the alert body.',
    table: {
      category: 'Alert text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Body text ends

  // Has links selection
  hasLinks: {
    name: 'hasLinks',
    description: 'Does the alert have links?',
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean'
      },
    },
  },// Has links ends

  // Link 1 text
  link1Text: {
    name: 'link1Text',
    description: "The alert's first link's text content.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

  // Link 1 text
  link1URL: {
    name: 'link1URL',
    description: "The alert's first link's URL.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

  // Link 1 text
  link2Text: {
    name: 'link2Text',
    description: "The alert's second link's text content.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

  // Link 1 text
  link2URL: {
    name: 'link2URL',
    description: "The alert's second link's URL.",
    table: {
      category: 'Alert links',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Link 1 text ends

}// Error alert ends