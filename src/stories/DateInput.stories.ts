
import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { DateInputComponent } from '../../projects/nedbank-design/src/lib/date-input/date-input.component';
import { FormLabelComponent } from '../../projects/nedbank-design/src/lib/form-label/form-label.component';
import { FormHelperTextComponent } from '../../projects/nedbank-design/src/lib/form-helper-text/form-helper-text.component';
import { TooltipComponent } from '../../projects/nedbank-design/src/lib/tooltip/tooltip.component';


export default {
  title: 'Date input',
  component: DateInputComponent,
  subcomponents: { TooltipComponent, FormLabelComponent, FormHelperTextComponent },

} as Meta;

const Template: Story<DateInputComponent> = (args: DateInputComponent) => ({
  component: DateInputComponent,
  props: args,
  moduleMetadata: {
    declarations: [DateInputComponent, TooltipComponent, FormLabelComponent, FormHelperTextComponent],
  },
});

// Date input
export const DateInput = Template.bind({});

DateInput.args = {
  // Label
  hasLabel: true,
  labelText: 'Lorem ipsum',
  // Tooltip
  hideTooltip: false,
  hasHeading: true,
  tooltipHeadingText: 'Lorem ipsum',
  tooltipText: 'Lorem ipsum',
  // Helper text
  hashelperText: true,
  helperText: 'Lorem ipsum',
  // States
  disabled: false,
  isValid: false,
  notValid: false,
};


DateInput.argTypes = {

  // Has label
  hasLabel: {
    name: 'hasLabel',
    description: 'Whether the checkbox has a top level label, eg. for checkbox groups.',
    table: {
      category: 'Label',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Has label ends

  // Label text
  labelText: {
    name: 'labelText',
    description: 'The text for the top level label, eg. for checkbox groups.',
    table: {
      category: 'Label',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Label text ends

  // Has tooltip
  hideTooltip: {
    name: 'hideTooltip',
    description: 'Whether the top level label contains a tooltip.',
    table: {
      category: 'Tooltip',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Has tooltip ends

  // Has tooltip heading
  hasHeading: {
    name: 'hasHeading',
    description: 'Whether the tooltip has a heading.',
    table: {
      category: 'Tooltip',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Has tooltip heading ends

  // Tooltip heading text
  tooltipHeadingText: {
    name: 'tooltipHeadingText',
    description: 'The text for the tooltip heading.',
    table: {
      category: 'Tooltip',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Tooltip heading text ends

  // Tooltip heading text
  tooltipText: {
    name: 'tooltipText',
    description: 'The text for the tooltip body copy.',
    table: {
      category: 'Tooltip',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Tooltip heading text ends
  
  // Has helper text
  hashelperText: {
    name: 'hashelperText',
    description: 'Show the helper text component.',
    table: {
      category: 'Helper text',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Has helper text ends

  // Tooltip heading text
  helperText: {
    name: 'helperText',
    description: 'The contents of the helper text.',
    table: {
      category: 'Helper text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Tooltip heading text ends

  // Disabled state
  disabled: {
    name: 'disabled',
    description: 'Is the conponent disabled?',
    table: {
      category: 'States',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Disabled state ends

  // Valid state
  isValid: {
    name: 'isValid',
    description: 'Is the component valid?',
    table: {
      category: 'States',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// valid state ends

  // Invalid state
  notValid: {
    name: 'notValid',
    description: 'Is the component invalid, or has an error occured?',
    table: {
      category: 'States',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Disabled state ends

}// Checkbox ends

// storiesOf('Date input', module)

//   .addDecorator(centered)

//   .addDecorator(
//     moduleMetadata({
//       declarations: [ DateInputComponent, FormLabelComponent, FormHelperTextComponent, TooltipComponent ]
//     })
//   )

//   .add('Date input', () => ({
//     template: `
//     <ui-date-input 
//       [hasLabel]="true"
//       labelText="Lorem ipsum" 
//       arialabel="Add a single date" 
//       [hideTooltip]="true" 
//       [hasHeading]="true" 
//       tooltipHeadingText="Lorem ipsum" 
//       tooltipText="Amet consectetur adipisicing elit." 
//       [hashelperText]="true" 
//       helperText="Lorem ipsum" 
//       [disabled]="false" 
//       [isValid]="false" 
//       [notValid]="false" 
//     ></ui-date-input>
//     `,
//     props: {
//       // labelText: text('Label text', 'Single date', 'General'),
//       // arialabel: text('Aria label text', 'Add a single date', 'General'),
//       // helperText: text('Helper Text', 'Default helper text', 'General'),
//     },
//   }),
//   {
//     'in-dsm': {
//       id: '5ec645395b0f901b0ed9de58',
//       componentPath: '../../projects/nedbank-design/src/lib/date-input/date-input.component'
//     }
//   }
// );
