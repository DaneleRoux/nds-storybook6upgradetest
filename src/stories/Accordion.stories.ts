import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { AccordionComponent } from '../../projects/nedbank-design/src/lib/accordion/accordion.component';

const mockAccordionData = [
  {
    accordionLink: 'Accordion link',
    accordionContent: `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempora, consequatur commodi vero earum nihil nemo eaque quisquam ipsum ullam, praesentium necessitatibus animi fugiat rem accusantium corrupti atque minima voluptatum saepe.`,
  },
  {
    accordionLink: 'Another accordion link',
    accordionContent: `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempora, consequatur commodi vero earum nihil nemo eaque quisquam ipsum ullam, praesentium necessitatibus animi fugiat rem accusantium corrupti atque minima voluptatum saepe.`,
  },
  {
    accordionLink: 'Why not 3 accordion links?',
    accordionContent: `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempora, consequatur commodi vero earum nihil nemo eaque quisquam ipsum ullam, praesentium necessitatibus animi fugiat rem accusantium corrupti atque minima voluptatum saepe.`,
  },
];

export default {
  title: 'Accordions',
  component: AccordionComponent,
} as Meta;

const Template: Story<AccordionComponent> = (args: AccordionComponent) => ({
  component: AccordionComponent,
  props: args,
  moduleMetadata: {
    declarations: [AccordionComponent],
  },
});

// Desktop accordion
export const Desktop = Template.bind({});
Desktop.args = {
  smallAccordion: false,
  openFirstAccorion: true,
  accordionData: mockAccordionData,
};

Desktop.argTypes = {

  // Large or condensed selection
  smallAccordion: {
    name: 'smallAccordion',
    description: 'Condensed accordions, to be used inside containers.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Large or condensed selection ends

  // Open 1st link selection
  openFirstAccorion: {
    name: 'openFirstAccorion',
    description: 'Is the first link and its associated panel open by default?',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Open 1st link selection ends

  // Accordion data set
  accordionData: {
    name: 'accordionData',
    description: 'The data set that populates the accordions.',
    table: {
      category: 'Data sets',
      defaultValue: {
        summary: 'none'
      },
      type: {
        summary: 'object',
      },
      disable: true
    },
  },// Accordion data set

}// Desktop accordion ends


// Condensed accordion
export const Condensed = Template.bind({});
Condensed.args = {
  smallAccordion: true,
  openFirstAccorion: true,
  accordionData: mockAccordionData,
};

Condensed.argTypes = {

  // Large or condensed selection
  smallAccordion: {
    name: 'smallAccordion',
    description: 'Condensed accordions, to be used inside containers.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Large or condensed selection ends

  // Open 1st link selection
  openFirstAccorion: {
    name: 'openFirstAccorion',
    description: 'Is the first link and its associated panel open by default?',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Open 1st link selection ends

  // Accordion data set
  accordionData: {
    name: 'accordionData',
    description: 'The data set that populates the accordions.',
    table: {
      category: 'Data sets',
      defaultValue: {
        summary: 'none'
      },
      type: {
        summary: 'object',
      },
      disable: true
    },
  },// Accordion data set

}// Condensed accordion ends
