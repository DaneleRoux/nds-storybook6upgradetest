import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { CheckboxComponent } from '../../projects/nedbank-design/src/lib/checkbox/checkbox.component';
import { FormLabelComponent } from '../../projects/nedbank-design/src/lib/form-label/form-label.component';
import { FormHelperTextComponent } from '../../projects/nedbank-design/src/lib/form-helper-text/form-helper-text.component';
import { TooltipComponent } from '../../projects/nedbank-design/src/lib/tooltip/tooltip.component';


export default {
  title: 'Checkbox',
  component: CheckboxComponent,
  subcomponents: { TooltipComponent, FormLabelComponent, FormHelperTextComponent },

} as Meta;

const Template: Story<CheckboxComponent> = (args: CheckboxComponent) => ({
  component: CheckboxComponent,
  props: args,
  moduleMetadata: {
    declarations: [CheckboxComponent, TooltipComponent, FormLabelComponent, FormHelperTextComponent],
  },
});


// Checkbox
export const Checkbox = Template.bind({});
Checkbox.args = {
  // Properties
  checkboxId: 'checkboxID',
  checkboxFor: 'checkboxID',
  checkboxText: 'Lorem ipsum',
  groupItemMargin: false,
  // Label
  hasLabel: true,
  labelText: 'Lorem ipsum',
  // Tooltip
  hideTooltip: false,
  hasHeading: true,
  tooltipHeadingText: 'Lorem ipsum',
  tooltipText: 'Lorem ipsum',
  // Helper text
  hashelperText: true,
  helperText: 'Lorem ipsum',
  // States
  disabled: false,
  // isValid: false,
  notValid: false,
};

Checkbox.argTypes = {

  // Checkbox ID
  checkboxId: {
    name: 'checkboxId',
    description: 'The ID attrubute assigned to the checkbox.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'checkboxID'
      },
      type: {
        summary: 'string',
      },
    },
  },// Checkbox ID 
  
  // Checkbox For
  checkboxFor: {
    name: 'checkboxFor',
    description: 'The FOR attrubute assigned to the checkbox.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'checkboxFor'
      },
      type: {
        summary: 'string',
      },
    },
  },// Checkbox For ends

  // Checkbox text
  checkboxText: {
    name: 'checkboxText',
    description: 'The text label for the checkbox (not to be confused with the group label).',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Checkbox text ends

  // Group margin
  groupItemMargin: {
    name: 'groupItemMargin',
    description: 'Applies a 15px margin between checkboxes in a group.',
    table: {
      category: 'Properties',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Group margin ends

  // Has label
  hasLabel: {
    name: 'hasLabel',
    description: 'Whether the checkbox has a top level label, eg. for checkbox groups.',
    table: {
      category: 'Label',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Has label ends

  // Label text
  labelText: {
    name: 'labelText',
    description: 'The text for the top level label, eg. for checkbox groups.',
    table: {
      category: 'Label',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Label text ends

  // Has tooltip
  hideTooltip: {
    name: 'hideTooltip',
    description: 'Whether the top level label contains a tooltip.',
    table: {
      category: 'Tooltip',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Has tooltip ends

  // Has tooltip heading
  hasHeading: {
    name: 'hasHeading',
    description: 'Whether the tooltip has a heading.',
    table: {
      category: 'Tooltip',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Has tooltip heading ends

  // Tooltip heading text
  tooltipHeadingText: {
    name: 'tooltipHeadingText',
    description: 'The text for the tooltip heading.',
    table: {
      category: 'Tooltip',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Tooltip heading text ends

  // Tooltip heading text
  tooltipText: {
    name: 'tooltipText',
    description: 'The text for the tooltip body copy.',
    table: {
      category: 'Tooltip',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Tooltip heading text ends
  
  // Has helper text
  hashelperText: {
    name: 'hashelperText',
    description: 'Show the helper text component.',
    table: {
      category: 'Helper text',
      defaultValue: {
        summary: 'true'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Has helper text ends

  // Tooltip heading text
  helperText: {
    name: 'helperText',
    description: 'The contents of the helper text.',
    table: {
      category: 'Helper text',
      defaultValue: {
        summary: 'Lorem ipsum'
      },
      type: {
        summary: 'string',
      },
    },
  },// Tooltip heading text ends

  // Disabled state
  disabled: {
    name: 'disabled',
    description: 'Is the conponent disabled?',
    table: {
      category: 'States',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Disabled state ends

  // Valid state
  // isValid: {
  //   name: 'isValid',
  //   description: 'Is the component valid?',
  //   table: {
  //     category: 'States',
  //     defaultValue: {
  //       summary: 'false'
  //     },
  //     type: {
  //       summary: 'boolean',
  //     },
  //   },
  // },// valid state ends

  // Invalid state
  notValid: {
    name: 'notValid',
    description: 'Is the component invalid, or has an error occured?',
    table: {
      category: 'States',
      defaultValue: {
        summary: 'false'
      },
      type: {
        summary: 'boolean',
      },
    },
  },// Disabled state ends

}// Checkbox ends

// storiesOf('Checkbox', module)
//   .addDecorator(centered)
//   .addDecorator(
//     moduleMetadata({
//       declarations: [CheckboxComponent, FormLabelComponent, FormHelperTextComponent, TooltipComponent]
//     })
//   )

//   .add('Checkbox', () => ({
//     template: `
//       <ui-checkbox 
//         checkboxId="checkboxid" 
//         checkboxFor="checkboxid" 
//         [groupItemMargin]="false"
//         [hasLabel]="true" 
//         labelText="Lorem ipsum" 
//         checkboxText="Option1"
//         arialabel="Lorem ipsum" 
//         [hideTooltip]="false" 
//         [hasHeading]="true" 
//         tooltipHeadingText="Lorem ipsum" 
//         tooltipText="Amet consectetur adipisicing elit." 
//         [hashelperText]="false" 
//         helperText="Lorem ipsum" 
//         [disabled]="false" 
//         [isValid]="false" 
//         [notValid]="false" 
//       ></ui-checkbox>
//     `,
//     props: {
//       // hasLabel: boolean('Does the checkbox need group a label?', false, 'General'),
//       // groupItemMargin: boolean('Checkbox in group margin bottom?', false, 'General'),
//       // labelText: text('Label text', 'Default label', 'General'),
//       // checkboxId: text('Checkbox Id', 'checkboxid', 'General'),
//       // checkboxFor: text('Checkbox For', 'checkboxid', 'General'),
//     },
//   }),
//   {
//     'in-dsm': {
//       id: '5eb3d593dbb8e41e0f591b21',
//       componentPath: '../../projects/nedbank-design/src/lib/checkbox/checkbox.component'
//     }
//   }
// );